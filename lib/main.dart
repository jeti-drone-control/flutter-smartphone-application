import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';

import 'package:jetter/config.dart';
import 'package:jetter/ui/screens/bluetooth-screen.dart';
import 'package:jetter/ui/screens/control-add.dart';
import 'package:jetter/ui/screens/control-overview.dart';
import 'package:jetter/ui/screens/control-screen.dart';
import 'package:jetter/ui/screens/main-screen.dart';
import 'package:jetter/ui/screens/telemetry-screen.dart';
import 'package:jetter/utils/logger.dart';

void main() {
  initializeLogger();
  GlobalConfiguration().loadFromMap(config);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) => MaterialApp(
        title: GlobalConfiguration().get("APP_TITLE"),
        initialRoute: '/',
        routes: {
          '/': (context) => MainScreen(),
          '/control': (context) => ControlScreen(),
          '/assignment': (context) => ControlOverview(),
          '/assignment/add': (context) => AddControlScreen(),
          '/telemetry': (context) => TelemetryScreen(),
          '/bluetooth': (context) => BluetoothScreen(),
        },
        theme: ThemeData(
          primarySwatch: Colors.blueGrey,
        ),
      );
}
