final Map<String, String> config = {
  "APP_NAME": "Jetter",
  "APP_TITLE": "Jetter",
};

const joystickTravelLimitPercent = 100.0;
const controlChannelLimitPercent = 125.0;
const controlChannels = 16;
