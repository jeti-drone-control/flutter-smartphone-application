import 'package:f_logs/f_logs.dart';

void log(String text, LogLevel logLevel) {
  print('[$logLevel] $text');
  // switch (logLevel) {
  //   case LogLevel.DEBUG:
  //     FLog.debug(text: text);
  //     break;
  //   case LogLevel.INFO:
  //     FLog.info(text: text);
  //     break;
  //   case LogLevel.WARNING:
  //     FLog.warning(text: text);
  //     break;
  //   case LogLevel.ERROR:
  //     FLog.error(text: text);
  //     break;
  //   case LogLevel.SEVERE:
  //     FLog.severe(text: text);
  //     break;
  //   case LogLevel.FATAL:
  //     FLog.fatal(text: text);
  //     break;
  //   default:
  //     break;
  // }
}

void initializeLogger() {
  LogsConfig config = FLog.getDefaultConfigurations()
    ..isDevelopmentDebuggingEnabled = true
    ..isLogsEnabled = true
    ..timestampFormat = TimestampFormat.TIME_FORMAT_FULL_3
    ..formatType = FormatType.FORMAT_CUSTOM
    ..fieldOrderFormatCustom = [
      FieldName.TIMESTAMP,
      FieldName.LOG_LEVEL,
      FieldName.CLASSNAME,
      FieldName.METHOD_NAME,
      FieldName.TEXT,
      FieldName.EXCEPTION,
      FieldName.STACKTRACE
    ]
    ..customOpeningDivider = "|"
    ..customClosingDivider = "|";

  FLog.applyConfigurations(config);
}
