import 'dart:math';

import 'package:flutter/material.dart';

class StickState extends State<StickWidget> {
  void Function(double xPosition, double yPosition) valueUpdateCallback;
  double joystickDiameter;
  double scale;
  double size;
  Color color;

  StickState(this.valueUpdateCallback, this.scale, this.color, this.size);

  num _getPosition(num position, num size, {num centerOffset = 0}) {
    final joystickCenterOffset = centerOffset / 2;
    if (position < joystickCenterOffset) {
      return 0.0;
    } else if (position + joystickCenterOffset >= size) {
      return size - centerOffset;
    } else {
      return position - joystickCenterOffset;
    }
  }

  double _getScaledValue(double localValue) {
    final scaledValue = (localValue) / (size) * scale;
  return ((scaledValue / (scale / 2).toDouble() - 1.0) * 100);
  }

  Offset _getLocalPosition(Offset tapPosition, num size) {
    final dx = _getPosition(tapPosition.dx, size);
    final dy = _getPosition(tapPosition.dy, size);
    valueUpdateCallback(_getScaledValue(dx), _getScaledValue(dy));
    return Offset(dx, dy);
  }

  @override
  Widget build(BuildContext context) {
    this.joystickDiameter = this.size / 5;
    Offset position = Offset(this.size / 2, this.size / 2);

    return Center(child: StatefulBuilder(
      builder: (context, setState) {
        return GestureDetector(
          onPanStart: (details) {
            setState(() => position = details.localPosition);
          },
          onPanEnd: (details) {
            setState(() => position = _getLocalPosition(
                Offset(this.size / 2, this.size / 2), this.size));
          },
          onPanUpdate: (details) {
            setState(() =>
                position = _getLocalPosition(details.localPosition, this.size));
          },
          child: Container(
            color: this.color,
            height: this.size,
            width: this.size,
            child: GridPaper(
              interval: this.size / 2,
              divisions: 2,
              subdivisions: 1,
              color: Colors.green,
              child: Stack(
                children: <Widget>[
                  Positioned(
                    top: _getPosition(position.dy, this.size,
                        centerOffset: this.joystickDiameter),
                    left: _getPosition(position.dx, this.size,
                        centerOffset: joystickDiameter),
                    child: Container(
                      width: joystickDiameter,
                      height: joystickDiameter,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle, color: Colors.black),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    ));
  }
}

// This might be a stateful widget
class StickWidget extends StatefulWidget {
  void Function(num xPosition, num yPosition) valueUpdateCallback;
  double joystickDiameter;
  double scale;
  double size;
  Color color;

  StickWidget(this.valueUpdateCallback, this.scale, this.color, this.size);

  @override
  State<StatefulWidget> createState() =>
      StickState(this.valueUpdateCallback, this.scale, this.color, this.size);
}
