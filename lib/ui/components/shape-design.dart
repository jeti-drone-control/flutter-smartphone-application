import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jetter/ui/components/function-chart.dart';

const debounceMilliseconds = 500;
const debounceDuration = const Duration(milliseconds: debounceMilliseconds);

class _ShapeDesignState extends State<ShapeDesign> {
  double ratio;
  double exponential;
  Timer _debounceTimer;

  _ShapeDesignState()
      : ratio = 0,
        exponential = 0 {
//    updateWidgetValues();
  }

  void updateWidgetValues() {
    this.widget.slope = this.ratio;
    this.widget.exponential = this.exponential;
  }


  void setRatio(double newValue) {
    if (_debounceTimer != null && _debounceTimer.isActive) _debounceTimer
        .cancel();
    this.setState(() => ratio = newValue);
    _debounceTimer = Timer(debounceDuration, () {});
    updateWidgetValues();
  }

  void setExponential(double newValue) {
    if (_debounceTimer != null && _debounceTimer.isActive) _debounceTimer
        .cancel();
      this.setState(() =>
      exponential = newValue);
    _debounceTimer = Timer(debounceDuration, () {});
    updateWidgetValues();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(12),
        child: Column(
          children: <Widget>[
            Text("Transfer function ratio [%]:"),
            Slider(
              label: "ratio",
              value: this.ratio,
              onChanged: this.setRatio,
              max: 100.0,
              min: -100.0,
            ),
            Slider(
              label: "expontial",
              value: this.exponential,
              onChanged: this.setExponential,
              max: 100.0,
              min: -100.0,
            ),
            FunctionChart(ratio, exponential),
          ],
        ));
  }
}

class ShapeDesign extends StatefulWidget {

  double exponential;
  double slope;

  @override
  State<StatefulWidget> createState() {
    return new _ShapeDesignState();
  }
}
