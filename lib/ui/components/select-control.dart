import 'package:flutter/material.dart';
import 'package:jetter/core/control/axis.dart';
import 'package:jetter/core/control/joystick.dart';
import 'package:jetter/core/control/positive-orientation.dart';
import 'package:jetter/ui/components/control-arrows.dart';

class _SelectControl extends State<SelectControl> {
  ControlAxis selectedControlAxis;

  Function onPressJoystickAxis(
      Joystick joystick, PositiveOrientation positiveOrientation) {
    final selectedAxis = getControlAxis(joystick, positiveOrientation);
    setState(() {
      selectedControlAxis = selectedAxis;
    });
    this.widget.selectedAxis = selectedAxis;
  }

  Function onPressJoystick(Joystick joystick) =>
      (PositiveOrientation positiveOrientation) =>
          {this.onPressJoystickAxis(joystick, positiveOrientation)};

  @override
  Widget build(BuildContext context) {
    List<ControlArrows> arrowSelection;
    if (selectedControlAxis == null) {
      arrowSelection = [
        ControlArrows(this.onPressJoystick(Joystick.LEFT)),
        ControlArrows(this.onPressJoystick(Joystick.RIGHT))
      ];
    } else {
      switch (selectedControlAxis) {
        case ControlAxis.LEFT_JOYSTICK_HORIZONTAL_AXIS:
          arrowSelection = [
            ControlArrows(this.onPressJoystick(Joystick.LEFT),
                selectedAxis: Axis.horizontal),
            ControlArrows(this.onPressJoystick(Joystick.RIGHT))
          ];
          break;
        case ControlAxis.LEFT_JOYSTICK_VERTICAL_AXIS:
          arrowSelection = [
            ControlArrows(this.onPressJoystick(Joystick.LEFT),
                selectedAxis: Axis.vertical),
            ControlArrows(this.onPressJoystick(Joystick.RIGHT))
          ];
          break;
        case ControlAxis.RIGHT_JOYSTICK_HORIZONTAL_AXIS:
          arrowSelection = [
            ControlArrows(this.onPressJoystick(Joystick.LEFT)),
            ControlArrows(this.onPressJoystick(Joystick.RIGHT),
                selectedAxis: Axis.horizontal)
          ];
          break;
        case ControlAxis.RIGHT_JOYSTICK_VERTICAL_AXIS:
          arrowSelection = [
            ControlArrows(this.onPressJoystick(Joystick.LEFT)),
            ControlArrows(this.onPressJoystick(Joystick.RIGHT),
                selectedAxis: Axis.vertical)
          ];
          break;
      }
    }

    return Container(
      padding: EdgeInsets.all(8),
      color: Colors.blue,
      child: Column(
        children: <Widget>[
          Text("Control  direction:"),
          Divider(
            height: 16,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: arrowSelection
          )
        ],
      ),
    );
  }
}

class SelectControl extends StatefulWidget {

  ControlAxis selectedAxis;

  @override
  State<StatefulWidget> createState() {
    return new _SelectControl();
  }
}
