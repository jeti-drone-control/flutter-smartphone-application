import 'dart:math';

import 'package:flutter/material.dart';
import 'package:jetter/core/control/positive-orientation.dart';

const arrowIconSize = 48.0;
const selectedColor = Colors.white;
const idleColor = Colors.black;

class ControlArrows extends StatelessWidget {
  final Function onPressJoystickAxis;
  final Axis selectedAxis;

  ControlArrows(this.onPressJoystickAxis, {this.selectedAxis});

  SizedBox renderIconButton(Icon icon, Function onPressed, Color color) {
    final button = new SizedBox(
        height: arrowIconSize,
        width: arrowIconSize,
        child: new IconButton(
          padding: new EdgeInsets.all(0.0),
          iconSize: arrowIconSize,
          icon: icon,
          color: color,
          onPressed: onPressed,
        ));
    return button;
  }

  Row renderOneIconRow(Icon icon, onPressed, Color color) {
    final row = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[renderIconButton(icon, onPressed, color)],
    );
    return row;
  }

  Row renderTwoIconButtons() => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          renderOneIconRow(Icon(Icons.arrow_back), () {
            this.onPressJoystickAxis(PositiveOrientation.LEFT);
          }, this.selectedAxis == Axis.horizontal ? selectedColor : idleColor),
          renderOneIconRow(Icon(Icons.arrow_forward), () {
            this.onPressJoystickAxis(PositiveOrientation.RIGHT);
          }, this.selectedAxis == Axis.horizontal ? selectedColor : idleColor),
        ],
      );

  @override
  Widget build(BuildContext context) {
    return Container(
      height: (arrowIconSize * 3).toDouble(),
      width: (arrowIconSize * 3).toDouble(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          renderOneIconRow(Icon(Icons.arrow_upward), () {
            this.onPressJoystickAxis(PositiveOrientation.UP);
          }, this.selectedAxis == Axis.vertical ? selectedColor : idleColor),
          renderTwoIconButtons(),
          renderOneIconRow(Icon(Icons.arrow_downward), () {
            this.onPressJoystickAxis(PositiveOrientation.DOWN);
          }, this.selectedAxis == Axis.vertical ? selectedColor : idleColor),
        ],
      ),
    );
  }
}
