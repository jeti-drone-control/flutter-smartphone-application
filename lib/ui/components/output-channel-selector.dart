import 'package:flutter/material.dart';
import 'package:jetter/config.dart';

class _OutputChannelSelectorState extends State<OutputChannelSelector> {

  int dropdownValue = 0;
  final channelItems = List.generate(controlChannels + 1,
      (index) => DropdownMenuItem(value: index, child: Text(index == 0 ? "" : "$index")));

  get channelIndex => dropdownValue - 1;

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text("Output channel:"),
          DropdownButton<int>(
            value: dropdownValue,
            icon: Icon(Icons.arrow_drop_down),
            iconSize: 24,
            elevation: 16,
            underline: Container(
              height: 2,
              color: Colors.deepPurpleAccent,
            ),
            onChanged: (int newValue) {
              setState(() {
                dropdownValue = newValue;
              });
              this.widget.selectedChannel = newValue;
            },
            items: channelItems
          ),
        ]);
  }
}

class OutputChannelSelector extends StatefulWidget {

  int selectedChannel;

  @override
  State<StatefulWidget> createState() {
    return new _OutputChannelSelectorState();
  }
}
