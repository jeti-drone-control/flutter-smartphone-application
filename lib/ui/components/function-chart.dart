import 'package:flutter/material.dart';
import 'package:jetter/config.dart';
import 'package:jetter/core/control/transferFunction.dart';
import 'package:fl_chart/fl_chart.dart';

class FunctionChart extends StatelessWidget {
  double ratio;
  double exponential;
  List<double> dataPoints;

  FunctionChart(this.ratio, this.exponential) {
    dataPoints = List.generate(201, (int index) => index.toDouble() - 100);
  }

  List<FlSpot> getData() {
    List<FlSpot> spots = List();
    for (var xValue in dataPoints) {
      final y =
          transferFunction(xValue.toDouble() / 100, ratio / 100, exponential) *
              100;
      spots.add(FlSpot(xValue, y));
    }

    return spots;
  }

  @override
  Widget build(BuildContext context) {
    final data = getData();
    return Container(
      child: LineChart(
        LineChartData(
            minY: -controlChannelLimitPercent,
            maxY: controlChannelLimitPercent,
            minX: -joystickTravelLimitPercent,
            maxX: joystickTravelLimitPercent,
            extraLinesData: ExtraLinesData(horizontalLines: [
              HorizontalLine(y: -100),
              HorizontalLine(y: 0),
              HorizontalLine(y: 100),
            ], verticalLines: [
              VerticalLine(x: -100),
              VerticalLine(x: 0),
              VerticalLine(x: 100)
            ]),
            lineBarsData: [
              LineChartBarData(
                  spots: data,
                  isCurved: false,
                  barWidth: 4,
                  colors: [
                    Colors.orange,
                  ],
                  belowBarData: BarAreaData(
                    show: false,
                  ),
                  dotData: FlDotData(
                    show: false,
                  )),
            ],
            gridData: FlGridData(
              show: false,
            ),
            borderData: FlBorderData(
              show: true,
            ),
            axisTitleData: FlAxisTitleData(
              show: true,
              leftTitle: AxisTitle(titleText: "Travel [%]"),
            ),
            titlesData: FlTitlesData(
                leftTitles: SideTitles(
                    showTitles: true,
                    getTitles: (value) {
                      if (value % 50 == 0 ||
                          value.abs() == controlChannelLimitPercent) {
                        return "$value";
                      }
                    }),
                bottomTitles: SideTitles(
                  showTitles: true,
                  interval: 5,
                  getTitles: (value) {
                    if (value % 50 == 0 ||
                        value.abs() == joystickTravelLimitPercent) {
                      return "$value";
                    }
                  },
                ))),
      ),
    );

  }
}
