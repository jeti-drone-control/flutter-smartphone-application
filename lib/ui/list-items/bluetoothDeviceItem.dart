import 'package:flutter/material.dart';

class BluetoothDeviceItem extends StatelessWidget {

  String name;
  String ssid;
  bool isConnected;
  Function onPressCall;

  BluetoothDeviceItem(this.name, this.ssid, this.isConnected, this.onPressCall);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80,
      margin: EdgeInsets.symmetric(horizontal: 8),
      decoration: BoxDecoration(color: Colors.grey, borderRadius: BorderRadius.all(Radius.circular(8))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[Text(name), Text(ssid)],
          ),
          RaisedButton(onPressed: onPressCall,child: Text(isConnected ? "Disconnect" : "Connect"),)
        ],
      ),
    );
  }

}