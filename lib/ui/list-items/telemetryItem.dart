import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jetter/core/telemetry/sensor.dart';
import 'package:jetter/core/telemetry/sensorMeasurement.dart';
import 'package:jetter/core/telemetry/telemetryStore.dart';
import 'package:jetter/ui/list-items/measurementItem.dart';
import 'package:jetter/ui/style/textStyles.dart';
import 'package:jetter/ui/style/widgetStyles.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';

class TelemetryItem extends StatelessWidget {
  final Sensor sensor;

  TelemetryItem(this.sensor) : super();

  Widget buildContent() => Observer(
      builder: (_) => Expanded(
            child: Container(
              padding: const EdgeInsets.only(left: 16, top: 4, bottom: 4),
              decoration: widgetStyles['telemetryItem/content/decoration'],
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    sensor.title ?? '',
//            textAlign: TextAlign.left,
                    style: textStyles['telemetryItem/displayName'],
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                          // sensor.value != null
                          //     ? num.parse(sensor.value.toStringAsFixed(5))
                          //         .toString()
                          //     :
                          '---',
                          style: textStyles['telemetryItem/value']),
                      SizedBox(
                        width: 10,
                      ),
                      Text(sensor.subtitle ?? '',
                          style: textStyles['telemetryItem/unit'])
                    ],
                  ),
                ],
              ),
            ),
          ));

  Widget buildSidePannelIndicator() => Container(
        width: widgetStyles['telemetryItem/sideIndicator/width'],
        decoration: widgetStyles['telemetryItem/sideIndicator/decoration'],
        child: Icon(widgetStyles['telemetryItem/sideIndicator/icon']),
      );

  List<Widget> buildMeasurements(
      ObservableList<SensorMeasurement> measurements) {
    return measurements
        .map(
          (measurement) => MeasurementItem(measurement),
        )
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        // height: widgetStyles['telemetryItem/height'],
        child: Observer(builder: (_) {
      final measurements = sensor.measurements;
      return ExpansionTile(
        title: Text(sensor.title),
        subtitle: Text(sensor.subtitle != null ? sensor.subtitle : ''),
        children: buildMeasurements(measurements),
      );
    })

        // Row(
        //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //   crossAxisAlignment: CrossAxisAlignment.stretch,
        //   children: <Widget>[
        //     buildContent(),
        //     buildSidePannelIndicator(),
        //   ],
        // ),
        );
  }
}
