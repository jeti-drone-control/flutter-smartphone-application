import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:jetter/core/telemetry/sensorFloatValue.dart';
import 'package:jetter/core/telemetry/sensorMeasurement.dart';
import 'package:jetter/core/telemetry/sensorValue.dart';
import 'package:jetter/core/telemetry/sensorValueType.dart';

class MeasurementItem extends StatelessWidget {
  final SensorMeasurement measurement;

  MeasurementItem(this.measurement);

  Widget buildActionButton() {
    return Observer(builder: (_) {
      return FlatButton(
          onPressed: measurement.isSubscribed
              ? measurement.unsubscribe
              : measurement.subscribe,
          child: Text(measurement.isSubscribed ? "Unsubscribe" : "Subscribe"));
    });
  }

  Widget buildLeadingWidget(SensorMeasurement measurement) {
    return Observer(
      builder: (BuildContext context) {
        SensorValue value = measurement.sensorValue;
        if (value == null) {
          return Text('---');
        }

        switch (value.valueType) {
          case SensorValueType.DATE:
          case SensorValueType.TIME:
          case SensorValueType.GPS:
            break;
          case SensorValueType.FLOAT:
            SensorFloatValue floatValue = value as SensorFloatValue;
            return Text('${floatValue.value.toStringAsFixed(2)}');
        }
        return Text('---');
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
        leading: Container(
          width: 50,
          child: buildLeadingWidget(measurement),
        ),
        title: Text(measurement.description),
        subtitle: Text(measurement.unit != null ? measurement.unit : ''),
        trailing: buildActionButton());

    // ListTile(
    //   title: Text("${measurement.description} [${measurement.unit}]"),
    // );
  }
}
