import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:jetter/core/connectivity/bluetooth-status.dart';
import 'package:jetter/core/connectivity/bluetoothStore.dart';
import 'package:jetter/core/connectivity/connectivityStore.dart';
import 'package:jetter/ui/widgets/bluetoothDeviceList.dart';

class BluetoothItem extends StatelessWidget {
  Widget buildStatusIndication() =>
      Observer(builder: (_) => Text(BluetoothStore.instance.status.toString()));

  Widget getButtonText(BluetoothStatus bluetoothStatus) {
    switch (bluetoothStatus) {
      case BluetoothStatus.CONNECTED:
      case BluetoothStatus.CONNECTING:
      case BluetoothStatus.UNAVAILABLE:
      case BluetoothStatus.OFF:
        return FlatButton(
          onPressed: null,
          child: Text("Start scan"),
        );
        break;
      case BluetoothStatus.SCANNING:
        return FlatButton(
          onPressed: () => BluetoothStore.instance.stopScan(),
          child: Text("Stop scan"),
        );
        break;
      case BluetoothStatus.IDLE:
        return RaisedButton(
          onPressed: () => BluetoothStore.instance.startScan(),
          child: Text("Start scan"),
        );
        break;
    }
  }

  Function getButtonAction(BluetoothStatus bluetoothStatus) {
    switch (bluetoothStatus) {
      case BluetoothStatus.CONNECTED:
      case BluetoothStatus.CONNECTING:
      case BluetoothStatus.UNAVAILABLE:
      case BluetoothStatus.OFF:
        return null;
      case BluetoothStatus.SCANNING:
        return BluetoothStore.instance.stopScan;
      case BluetoothStatus.IDLE:
        return BluetoothStore.instance.startScan;
    }
  }

  Container _buildCircle(Color color) => Container(
        decoration: BoxDecoration(color: color, shape: BoxShape.circle),
        height: 20,
        width: 20,
      );

  Widget _buildDisconnectedIndicator() => Observer(builder: (_) {
        bool isOn = [
          BluetoothStatus.IDLE,
          BluetoothStatus.SCANNING,
          BluetoothStatus.UNAVAILABLE,
          BluetoothStatus.OFF
        ].contains(BluetoothStore.instance.status);
        return Container(
          decoration: BoxDecoration(
              color: isOn ? Colors.red : Colors.grey, shape: BoxShape.circle),
          height: 20,
          width: 20,
        );
      });

  Widget _buildProgressIndicator() => Observer(builder: (_) {
        final isOn = ([BluetoothStatus.CONNECTED, BluetoothStatus.CONNECTING]
                .contains(BluetoothStore.instance.status)) &&
            !ConnectivityStore.instance.isOnline;
        return Container(
          decoration: BoxDecoration(
              color: isOn ? Colors.yellow : Colors.grey,
              shape: BoxShape.circle),
          height: 20,
          width: 20,
        );
      });

  Widget _buildIsOnlineIndicator() => Observer(builder: (_) {
        return Container(
          decoration: BoxDecoration(
              color: ConnectivityStore.instance.isOnline
                  ? Colors.green
                  : Colors.grey,
              shape: BoxShape.circle),
          height: 20,
          width: 20,
        );
      });

  bool _isGreenOn(bool isConnectiviyOnline) {
    return isConnectiviyOnline;
  }

  Widget _getConnectionStatusIndicator() {
    return Container(
        child: Column(
      children: [
        _buildDisconnectedIndicator(),
        _buildProgressIndicator(),
        _buildIsOnlineIndicator()
      ],
    ));
  }

  @override
  Widget build(BuildContext context) => ExpansionTile(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [Text("Bluetooth"), _getConnectionStatusIndicator()],
        ),
        children: [
          Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [Text("Status"), buildStatusIndication()],
              ),
              FlatButton(
                  onPressed: BluetoothStore.instance.readValue,
                  child: Text("Read from device")),
              BluetoothDeviceList(),
              Observer(builder: (_) {
                BluetoothStatus bluetoothStatus =
                    BluetoothStore.instance.status;
                return FlatButton(
                  onPressed: getButtonAction(bluetoothStatus),
                  child: getButtonText(bluetoothStatus),
                );
              })
            ],
          )
        ],
      );
}
