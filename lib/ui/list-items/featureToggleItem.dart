import 'package:f_logs/model/flog/log_level.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:jetter/core/control/controlStore.dart';
import 'package:jetter/core/telemetry/telemetryStore.dart';
import 'package:jetter/utils/logger.dart';

class FeatureToggleItem extends StatelessWidget {
  void changeTelemetryState(bool isActive) {
    log("[FeatureToggleItem] Change telemetry active to $isActive",
        LogLevel.DEBUG);
    TelemetryStore.instance.setTelemetryActiveState(isActive);
  }

  void changeControlState(bool isActive) {
    log("[FeatureToggleItem] Change control active to $isActive",
        LogLevel.DEBUG);
    ControlStore.instance.changeControlState(isActive);
  }

  Widget buildTelemetryToggle() => Observer(
        builder: (_) => Switch(
          onChanged: changeTelemetryState,
          value: TelemetryStore.instance.isTelemetryActive,
        ),
      );

  Widget buildControlToggle() => Observer(
      builder: (_) => Switch(
            onChanged: changeControlState,
            value: ControlStore.instance.isControlActive,
          ));

  @override
  Widget build(BuildContext context) => ExpansionTile(
        title: Text("Features"),
        children: [
          ListTile(
              leading: Text("Telemetry"), trailing: buildTelemetryToggle()),
          ListTile(leading: Text("Control"), trailing: buildControlToggle())
        ],
      );
}
