import 'package:f_logs/f_logs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:jetter/core/connectivity/bluetooth-status.dart';
import 'package:jetter/core/connectivity/bluetoothStore.dart';
import 'package:jetter/core/connectivity/connectivityStore.dart';
import 'package:jetter/utils/logger.dart';

class ConnectivityItem extends StatelessWidget {
  Function getOnPressed(bool pingPongEnabled, bool isConnected) {
    if (!isConnected) return null;

    return (pingPongEnabled)
        ? ConnectivityStore.instance.stopPingPong
        : ConnectivityStore.instance.startPingPong;
  }

  Widget getButtonChild(bool pingPongEnabled) =>
      pingPongEnabled ? Text("Stop") : Text("Start");

  void resetSystems() {
    log("[Connectivity Item] Pressed reset", LogLevel.DEBUG);
    BluetoothStore.instance.resetSystems();
  }

  @override
  Widget build(BuildContext context) => ExpansionTile(
        title: Text("Connectivity"),
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Reset systems"),
              Observer(
                  builder: (_) => FlatButton(
                        onPressed: BluetoothStore.instance.status ==
                                BluetoothStatus.CONNECTED
                            ? resetSystems
                            : null,
                        child: Text("Reset"),
                      ))
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Ping pong"),
              Observer(
                  builder: (_) => FlatButton(
                      onPressed: getOnPressed(
                          ConnectivityStore.instance.pingPongEnabled,
                          BluetoothStore.instance.status ==
                              BluetoothStatus.CONNECTED),
                      child: getButtonChild(
                          ConnectivityStore.instance.pingPongEnabled)))
            ],
          ),
        ],
      );
}
