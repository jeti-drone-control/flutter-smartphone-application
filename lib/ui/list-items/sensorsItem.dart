import 'package:f_logs/model/flog/log_level.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:jetter/core/telemetry/telemetryStore.dart';
import 'package:jetter/utils/logger.dart';

class SensorsItem extends StatelessWidget {
  void startScan() {
    log("[SensorsItem] Start scan", LogLevel.DEBUG);
    TelemetryStore.instance.startScan();
  }

  void stopScan() {
    log("[SensorsItem] Stop scan>", LogLevel.DEBUG);
    TelemetryStore.instance.stopScan();
  }

  Widget getButtonText(bool isScanning) {
    return isScanning ? Text("Stop scan") : Text("Start scan");
  }

  Function getButtonAction(bool isActive, bool isScanning) {
    if (!isActive) {
      return null;
    }

    return TelemetryStore.instance.isScanning ? stopScan : startScan;
  }

  // Widget getIdentifiedSensorsButton() => Observer(
  //     builder: (_) => FlatButton(
  //         onPressed: TelemetryStore.instance.isScanning
  //             ? TelemetryStore.instance.pullIdentifiedSensors
  //             : null,
  //         child: Text("Pull")));

  Widget getTrailingButton() => Observer(
      builder: (_) => FlatButton(
          onPressed: getButtonAction(TelemetryStore.instance.isTelemetryActive,
              TelemetryStore.instance.isScanning),
          child: getButtonText(TelemetryStore.instance.isScanning)));

  @override
  Widget build(BuildContext context) => ExpansionTile(
        title: Text("Sensors"),
        children: [
          ListTile(
              leading: Text("Scan sensors"), trailing: getTrailingButton()),
          // ListTile(
          //     leading: Text("Get identified sensors"),
          //     trailing: getIdentifiedSensorsButton())
        ],
      );
}
