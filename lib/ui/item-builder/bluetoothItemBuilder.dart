import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:jetter/core/connectivity/bluetoothStore.dart';
import 'package:jetter/ui/list-items/bluetoothDeviceItem.dart';

IndexedWidgetBuilder BluetoothItemBuilder = (BuildContext context, int index) {
  return Observer(
    builder: (BuildContext context) {
      final devices = BluetoothStore.instance.devices;
      final ssid = devices.keys.toList()[index];
      final isConnected = BluetoothStore.instance.connectedDevice != null &&
          BluetoothStore.instance.connectedDevice.id.id == ssid;
      return BluetoothDeviceItem(devices[ssid].name, ssid, isConnected,
          () => {BluetoothStore.instance.connectDevice(ssid)});
    },
  );
};
