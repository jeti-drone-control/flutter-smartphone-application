import 'package:flutter/material.dart';

const Map widgetStyles = {
  /* Telemetry Screen */
  'telemetryScreen/color': Colors.black12,
  'telemetryScreen/itemPadding':
  EdgeInsets.symmetric(horizontal: 2, vertical: 12),
  /* Control Screen */
  'controlScreen/color': Colors.black12,
  'controlScreen/padding': EdgeInsets.symmetric(vertical: 16, horizontal: 0),
  'controlScreen/stickPannel/height': 100.0,
  /* Telemetry Item */
  'telemetryItem/height': 85.0,
  'telemetryItem/padding': const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
  'telemetryItem/color': Colors.white,
  'telemetryItem/sideIndicator/width': 24.0,
  'telemetryItem/sideIndicator/icon': Icons.arrow_forward_ios,
  'telemetryItem/sideIndicator/decoration': const BoxDecoration(
      borderRadius: BorderRadius.only(
          topRight: Radius.circular(8.0), bottomRight: Radius.circular(8.0)),
      color: Colors.black38
  ),
  'telemetryItem/content/decoration': const  BoxDecoration(
    borderRadius: BorderRadius.only(
        topLeft: Radius.circular(8.0),
        bottomLeft: Radius.circular(8.0)),
    color: Colors.black12,
  ),
};
