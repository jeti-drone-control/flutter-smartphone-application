
import 'package:flutter/material.dart';

final Map<String, TextStyle> textStyles  = {
  'telemetryItem/displayName': TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
  'telemetryItem/value': TextStyle(fontSize: 32),
  'telemetryItem/unit': TextStyle(fontSize: 24),


};