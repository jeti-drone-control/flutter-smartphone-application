import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:jetter/core/connectivity/bluetooth-status.dart';
import 'package:jetter/core/connectivity/bluetoothStore.dart';
import 'package:jetter/ui/item-builder/bluetoothItemBuilder.dart';
import 'package:jetter/ui/list-items/bluetoothDeviceItem.dart';

class BluetoothDeviceList extends StatelessWidget {
  Widget buildDeviceList(BluetoothStatus bluetoothStatus, int length) {
    if (bluetoothStatus == BluetoothStatus.CONNECTED) {
      BluetoothDevice connectedDevice = BluetoothStore.instance.connectedDevice;
      return BluetoothDeviceItem(connectedDevice.name, connectedDevice.id.id,
          true, BluetoothStore.instance.disconnect);
    } else if (bluetoothStatus == BluetoothStatus.CONNECTING ||
        (bluetoothStatus == BluetoothStatus.SCANNING && length > 0)) {
      return Container(
          height: 300,
          child: ListView.separated(
            itemCount: length,
            itemBuilder: BluetoothItemBuilder,
            separatorBuilder: (BuildContext context, int index) =>
                const Divider(),
          ));
    }

    return Container();
  }

  @override
  Widget build(BuildContext context) => Observer(
      builder: (context) => buildDeviceList(BluetoothStore.instance.status,
          BluetoothStore.instance.devices.length));
}
