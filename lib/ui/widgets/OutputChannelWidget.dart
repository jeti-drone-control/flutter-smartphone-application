import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:jetter/core/control/controlStore.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class OutputChannelWidget extends StatelessWidget {
  Widget buildChannelIndicator(int channelKey) {
    return Row(children: [
      Container(
        alignment: Alignment.center,
        width: 30,
        child: Text("$channelKey"),
      ),
      Expanded(child: Observer(
        builder: (_) {
          final channel = ControlStore.instance.channelValues[channelKey];
          final percentage = (channel / 100 + 1) / 2;
          return new LinearPercentIndicator(
            animation: true,
            animationDuration: 0,
            lineHeight: 20.0,
            percent: percentage,
            center: Text("${channel.round()} %"),
            linearStrokeCap: LinearStrokeCap.butt,
            progressColor: Colors.red,
          );
        },
      ))
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (BuildContext context) {
        final outputChannels = ControlStore.instance.channelValues;
        final channelKeys = outputChannels.keys.toList();
        channelKeys.sort();
        return ListView.separated(
            itemCount: outputChannels.length,
            separatorBuilder: (context_, index) => Divider(
                  height: 12,
                ),
            itemBuilder: (_, int index) {
              return buildChannelIndicator(channelKeys[index]);
            });
      },
    );
  }
}
