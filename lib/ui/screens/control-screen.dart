import 'package:flutter/material.dart';
import 'package:jetter/core/control/axis.dart';
import 'package:jetter/core/control/controlStore.dart';
import 'package:jetter/ui/components/stick-widget.dart';
import 'package:jetter/ui/style/widgetStyles.dart';
import 'package:jetter/ui/widgets/OutputChannelWidget.dart';

class ControlScreen extends StatelessWidget {
  Widget buildStick(
          ControlAxis xAxis,
          ControlAxis yAxis,
          Color color,
          Orientation orientation,
          double size) =>
      StickWidget((scaledX, scaledY) {
        ControlStore.instance.updateAxisValue(xAxis, scaledX);
        ControlStore.instance.updateAxisValue(yAxis, scaledY);
      }, 4096, color, size);


  Widget buildContent(Orientation orientation, Size size) {
    final stickSize =
        (orientation == Orientation.portrait ? size.width / 2 : size.height/ 2);

    final List<Widget> columnChildren = [
      Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Expanded(
          child: buildStick(
              ControlAxis.LEFT_JOYSTICK_HORIZONTAL_AXIS,
              ControlAxis.LEFT_JOYSTICK_VERTICAL_AXIS,
              Colors.red,
              orientation,
              stickSize),
        ),
        Expanded(
          child: buildStick(
              ControlAxis.RIGHT_JOYSTICK_HORIZONTAL_AXIS,
              ControlAxis.RIGHT_JOYSTICK_VERTICAL_AXIS,
              Colors.green,
              orientation,
              stickSize),
        ),
      ])
    ];
    if (orientation == Orientation.portrait) {
      columnChildren.insert(0, Expanded(child: OutputChannelWidget()));
    }

    return Column(
        mainAxisAlignment: columnChildren.length > 1
            ? MainAxisAlignment.spaceBetween
            : MainAxisAlignment.end,
        children: columnChildren);
  }

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: BackButton(
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          actions: <Widget>[
            PopupMenuButton(itemBuilder: (BuildContext context) {
              return [
                PopupMenuItem(
                    value: "/assignment/add",
                    child: FlatButton(
                      onPressed: () {
                        Navigator.pushNamed(context, "/assignment/add");
                      },
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.add,
                            color: Colors.black,
                          ),
                          Text("Add assignment"),
                        ],
                      ),
                    )),
                PopupMenuItem(
                    value: "clear",
                    child: FlatButton(
                      onPressed: () {
                        ControlStore.instance.channelAssignments.clear();
                        ControlStore.instance.channelValues.clear();
                      },
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.clear,
                            color: Colors.black,
                          ),
                          Text("Clear assignments"),
                        ],
                      ),
                    )),
                PopupMenuItem(
                    value: "/assignment",
                    child: FlatButton(
                      onPressed: () {
                        Navigator.pushNamed(context, '/assignment');
                      },
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.assignment,
                            color: Colors.black,
                          ),
                          Text("Show assignments"),
                        ],
                      ),
                    )),
              ];
            })
          ],
        ),
        body: Container(
          color: widgetStyles['controlScreen/color'],
          padding: widgetStyles['controlScreen/padding'],
          child: buildContent(
              MediaQuery.of(context).orientation, MediaQuery.of(context).size),
        ));
  }
}
