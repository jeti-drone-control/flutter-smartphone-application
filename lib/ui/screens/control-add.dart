import 'package:flutter/material.dart';
import 'package:jetter/core/control/channelAssignmentConfig.dart';
import 'package:jetter/core/control/controlStore.dart';
import 'package:jetter/ui/components/output-channel-selector.dart';
import 'package:jetter/ui/components/select-control.dart';
import 'package:jetter/ui/components/shape-design.dart';

class AddControlScreen extends StatelessWidget {
  final shapeDesign = ShapeDesign();
  final outputChannelSelector = OutputChannelSelector();
  final selectControlChannel = SelectControl();

  Row buildConfirmButton(BuildContext context) => Row(
        children: <Widget>[
          Expanded(
            child: FlatButton(
              color: Colors.blue,
              onPressed: this.onPressConfirm(context),
              child: Text('Confirm'),
            ),
          )
        ],
      );

  Function onPressConfirm(BuildContext context) {
    return () {
      if (selectControlChannel.selectedAxis == null) {
        AlertDialog();
        return;
      }

      if (outputChannelSelector.selectedChannel == 0) {
        AlertDialog();
        return;
      }

      final slope =
          shapeDesign.slope == null ? 0.toDouble() : shapeDesign.slope;
      final exponential = shapeDesign.exponential == null
          ? 0.toDouble()
          : shapeDesign.exponential;

      final config = ChannelAssignmentConfig(slope, exponential);
      ControlStore.instance.addChannelAssignment(
          selectControlChannel.selectedAxis,
          outputChannelSelector.selectedChannel,
          config);

      Navigator.pop(context);
    };
  }

  Function navigateBack(BuildContext context) {
    return () {
      Navigator.pop(context);
    };
  }

  Widget buildHeader(BuildContext context) => Row(
        children: <Widget>[
          FloatingActionButton(
            child: Icon(Icons.arrow_back),
            onPressed: navigateBack(context),
          )
        ],
      );

  buildContent(BuildContext context) {
    final listItems = [
      selectControlChannel,
      shapeDesign,
      outputChannelSelector,
      buildConfirmButton(context)
    ];
    return Expanded(
        child: ListView.builder(
            itemCount: listItems.length,
            itemBuilder: (BuildContext context, int index) {
              return listItems[index];
            }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[buildHeader(context), buildContent(context)],
      ),
    );
  }
}
