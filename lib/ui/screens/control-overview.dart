import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:jetter/core/control/axis.dart' as Control;
import 'package:jetter/core/control/controlStore.dart';

var count = 0;

class ControlOverview extends StatelessWidget {
  Function actionButtonPressed(BuildContext context) {
    return () {
      Navigator.pushNamed(context, '/assignment/add');
    };
  }

  Widget buildHeader(BuildContext context) => Row(
        children: <Widget>[
          FloatingActionButton(
            child: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            },
          )
        ],
      );

  @override
  Widget build(BuildContext context) {
    final items = ["Text", "Text", "Text"];
    return Scaffold(
        body: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            buildHeader(context),
            Expanded(child: Observer(builder: (_) {
              final assignments = ControlStore.instance.channelAssignments;
              return assignments.length == 0
                  ? Container()
                  : ListView.builder(
                      itemCount: assignments.length,
                      padding: EdgeInsets.all(16),
                      itemBuilder: (BuildContext context, int index) {
                        return Text("${assignments[index]}");
                      });
            }))
          ],
        ));
  }
}
