import 'package:flutter/material.dart';
import 'package:jetter/ui/list-items/bluetoothItem.dart';
import 'package:jetter/ui/list-items/connectivityItem.dart';
import 'package:jetter/ui/list-items/featureToggleItem.dart';
import 'package:jetter/ui/list-items/sensorsItem.dart';

class BluetoothScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: BackButton(
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: ListView(
          children: [
            BluetoothItem(),
            Divider(),
            ConnectivityItem(),
            Divider(),
            FeatureToggleItem(),
            Divider(),
            SensorsItem()
          ],
        )

        // Container(
        //   child: Column(
        //     children: <Widget>[
        //       SizedBox(
        //         width: double.infinity,
        //         child: Observer(builder: (_) {
        //           String scanButtonLabel;
        //           void Function() onPressed;
        //           switch (BluetoothStore.instance.status) {
        //             case BluetoothStatus.SCANNING:
        //               scanButtonLabel = "Stop scanning";
        //               onPressed = BluetoothStore.instance.stopScan;
        //               break;
        //             case BluetoothStatus.IDLE:
        //               scanButtonLabel = "Start scanning";
        //               onPressed = BluetoothStore.instance.startScan;
        //               break;
        //             case BluetoothStatus.CONNECTED:
        //               scanButtonLabel =
        //                   "Disconnect ${BluetoothStore.instance.connectedDevice.name}";
        //               onPressed = BluetoothStore.instance.disconnect;
        //               break;
        //             case BluetoothStatus.CONNECTING:
        //               scanButtonLabel = "Connecting";
        //               onPressed = null;
        //               break;
        //             default:
        //               print(BluetoothStore.instance.status);
        //               scanButtonLabel = "Unknown bluetooth status (disables)";
        //           }
        //           return RaisedButton(
        //               color: Colors.red,
        //               child: Observer(
        //                 builder: (_) {
        //                   return Text(
        //                       "$scanButtonLabel (${BluetoothStore.instance.status})");
        //                 },
        //               ),
        //               onPressed: onPressed);
        //         }),
        //       ),
        //       Expanded(
        //         child: Observer(
        //           builder: (_) => ListView.separated(
        //             itemCount: BluetoothStore.instance.devices.length,
        //             itemBuilder: BluetoothItemBuilder,
        //             separatorBuilder: (BuildContext context, int index) =>
        //                 const Divider(),
        //           ),
        //         ),
        //       )
        //     ],
        //   ),
        // )
        );
  }
}
