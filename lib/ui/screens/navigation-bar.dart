
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class _NavigationBar extends State<NavicationBar> {

  int _selectedIndex =0 ;



  var icons = [
    BottomNavigationBarItem(backgroundColor: Colors.blue, icon: Icon(Icons.add), title: Text('blue')),
    BottomNavigationBarItem(backgroundColor: Colors.red, icon: Icon(Icons.add), title: Text('red')),
    BottomNavigationBarItem(backgroundColor: Colors.green, icon: Icon(Icons.add), title: Text('green')),
  ];

  void _selectTab(int index ) {
    log('selected: ${index}');
    this.setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) => BottomNavigationBar(
      items: icons,
      onTap: _selectTab,
      currentIndex: _selectedIndex,
    );

}
class NavicationBar extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _NavigationBar();

}