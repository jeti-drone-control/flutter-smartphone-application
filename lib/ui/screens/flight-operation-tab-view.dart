import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:jetter/ui/screens/position-screen.dart';
import 'package:jetter/ui/screens/telemetry-screen.dart';

import 'control-screen.dart';

const icons = [
  Tab(icon: Icon(Icons.link), text: 'Telemetry'),
  Tab(icon: Icon(Icons.satellite), text: 'Control'),
  Tab(icon: Icon(Icons.gesture), text: 'GPS'),
];

var tabs = [
  TelemetryScreen(),
  ControlScreen(),
  PositionScreen(),
];

class FlightOperationTabView extends DefaultTabController {
  FlightOperationTabView()
      : super(
            length: tabs.length,
            child: Scaffold(
//    bottomNavigationBar: BottomNavigationBar(
//      items: icons,
//      onTap: ,
//    ),
              appBar: AppBar(
                  bottom: TabBar(tabs: icons),
                  title: Center(
                    child: Text(GlobalConfiguration().get("APP_TITLE")),
                  )),
              body: TabBarView(
                children: tabs,
              ),
            ));
}
