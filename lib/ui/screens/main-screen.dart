import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:jetter/ui/screens/bluetooth-screen.dart';
import 'package:jetter/ui/screens/control-screen.dart';
import 'package:jetter/ui/screens/flight-operation-tab-view.dart';
import 'package:jetter/ui/screens/navigation-bar.dart';
import 'package:jetter/ui/screens/position-screen.dart';
import 'package:jetter/ui/screens/telemetry-screen.dart';

class MainScreen extends StatelessWidget {
  Widget _buildPortraitLayout() => Column();

  Widget _buildButton(
          BuildContext context, String assetImagePath, String navigationPath) =>
      Expanded(
          child: Container(
              padding: EdgeInsets.symmetric(vertical: 16, horizontal: 32),
              child: RaisedButton(
                  color: Colors.lightBlueAccent,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12))),
                  onPressed: () {
                    Navigator.pushNamed(context, navigationPath);
                  },
                  child: Stack(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.all(16),
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(assetImagePath),
                                fit: BoxFit.fitHeight)),
                      ),
                    ],
                  ))));

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(
      ),
      body: Center(
          child: Container(
        padding: EdgeInsets.symmetric(vertical: 12),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            _buildButton(context, 'images/icons/control.png', '/control'),
            _buildButton(context, 'images/icons/meter.png', '/telemetry'),
            _buildButton(context, 'images/icons/wheel.png', '/bluetooth'),
          ],
        ),
      )));
}
