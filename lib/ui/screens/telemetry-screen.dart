import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:jetter/core/telemetry/telemetryStore.dart';
import 'package:jetter/ui/list-items/telemetryItem.dart';
import 'package:jetter/ui/style/widgetStyles.dart';

class TelemetryScreen extends StatelessWidget {
  Widget buildList() {
    return Container(
      color: widgetStyles['telemetryScreen/color'],
      child: Observer(builder: (_) {
        final sensors = TelemetryStore.instance.sensors;
        return ListView.separated(
            padding: widgetStyles['telemetryScreen/itemPadding'],
            itemCount: sensors.length,
            separatorBuilder: (BuildContext context, int index) => Divider(
                  thickness: 1.0,
                ),
            itemBuilder: (BuildContext context, int index) {
              return TelemetryItem(sensors[index]);
            });
      }),
    );
  }

  Widget build(BuildContext context) {
    var sensors = TelemetryStore.instance.sensors;
    log(sensors.toString());
    return Scaffold(
        appBar: AppBar(
          leading: BackButton(
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: buildList());
  }
}
