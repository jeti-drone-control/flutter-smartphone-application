import 'package:built_value/built_value.dart';
import 'package:built_collection/built_collection.dart';


class ChannelAssignmentConfig {

  final double exponential;
  final double slope;

  ChannelAssignmentConfig(this.slope, this.exponential);
}

