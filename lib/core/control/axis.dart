import 'package:flutter/material.dart';
import 'package:jetter/core/control/joystick.dart';
import 'package:jetter/core/control/positive-orientation.dart';

enum ControlAxis {
  LEFT_JOYSTICK_HORIZONTAL_AXIS,
  LEFT_JOYSTICK_VERTICAL_AXIS,
  RIGHT_JOYSTICK_HORIZONTAL_AXIS,
  RIGHT_JOYSTICK_VERTICAL_AXIS
}


ControlAxis getControlAxis(Joystick joystick, PositiveOrientation positiveOrientation) {
  if (joystick == Joystick.LEFT) {
    switch(positiveOrientation) {
      case PositiveOrientation.RIGHT:
      case PositiveOrientation.LEFT:
        return ControlAxis.LEFT_JOYSTICK_HORIZONTAL_AXIS;
      case PositiveOrientation.UP:
      case PositiveOrientation.DOWN:
      return ControlAxis.LEFT_JOYSTICK_VERTICAL_AXIS;
    }
  }

  if (joystick == Joystick.RIGHT) {
    switch(positiveOrientation) {
      case PositiveOrientation.RIGHT:
      case PositiveOrientation.LEFT:
        return ControlAxis.RIGHT_JOYSTICK_HORIZONTAL_AXIS;
      case PositiveOrientation.UP:
      case PositiveOrientation.DOWN:
        return ControlAxis.RIGHT_JOYSTICK_VERTICAL_AXIS;
    }
  }
}
