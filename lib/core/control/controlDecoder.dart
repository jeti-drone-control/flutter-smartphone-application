import 'package:f_logs/model/flog/log_level.dart';
import 'package:jetter/core/control/controlDecoderDelegate.dart';
import 'package:jetter/core/protocol-utils/abstract_payload_decoder.dart';
import 'package:jetter/core/protocol-utils/protocolConstants.dart';
import 'package:jetter/utils/logger.dart';

class ControlDecoder extends PayloadDecoder {
  final ControlDecoderDelegate _delegate;

  ControlDecoder(this._delegate) : super(controlProtocolIdentifier);

  @override
  void handleContent(List<int> buffer) {
    log('[ControlDecoder] received buffer with content type ${buffer[0]}',
        LogLevel.DEBUG);
    switch (buffer[0]) {
      case 0:
        return _delegate.receivedNack();
      case 255:
        return _delegate.receivedAck();
    }
  }
}
