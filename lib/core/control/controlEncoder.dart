import 'dart:typed_data';

import 'package:f_logs/model/flog/log_level.dart';
import 'package:flutter/services.dart';
import 'package:jetter/core/protocol-utils/abstract_payload_encoder.dart';
import 'package:jetter/core/protocol-utils/protocolConstants.dart';
import 'package:jetter/utils/logger.dart';

class ControlEncoder extends PayloadEncoder {
  ControlEncoder() : super();

  @override
  int get protocolIdentifier => controlProtocolIdentifier;

  List<int> encodeChannelValues(Map<int, double> channelValues) {
    List<int> buffer = List<int>();
    buffer.add(controlProtocolValues);

    Set<int> keys = channelValues.keys.toSet();
    if (keys.length != channelValues.length) {
      log("[ControlEncoder] Found multiple assignments to the same channel ${channelValues.keys}",
          LogLevel.DEBUG);
    }

    ByteData bytes;
    int value;
    for (int key in keys) {
      buffer.add(key - 1); // assumes counting from 1 to 16

      bytes = new ByteData(4);
      bytes.setFloat32(0, channelValues[key], Endian.little);
      value = bytes.getUint32(0);
      buffer.addAll([
        (value >> 24) & 0xFF,
        (value >> 16) & 0xFF,
        (value >> 8) & 0xFF,
        value & 0xFF
      ]);

      print("$key : ${channelValues[key]}, ${bytes.getUint32(0)}");
    }

    return encodeFrame(buffer);
  }

  List<int> encodeControlActivation() {
    return encodeFrame([controlProtocolActivate]);
  }

  List<int> encodeControlDeactivation() {
    return encodeFrame([controlProtocolDectivate]);
  }
}
