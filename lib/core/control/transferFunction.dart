import 'dart:math';
import 'dart:core';

double transferFunction(double x, double ratio, double exponential) {
  final k = (exponential / 10.0).roundToDouble();
  if (k == 0) return ratio * x;
  if (k < 0) {
    if (x < 0) {
      return ratio *
          -(1 - (pow(1 - x.abs(), 3) * (-k - 1) + (1 - x.abs())) / -k);
    } else {
      return ratio * (1 - (pow(1 - x, 3) * (-k - 1) + (1 - x)) / -k);
    }
  } else {
    if (x < 0) {
      return ratio * -((pow(x.abs(), 3) * (k - 1) + x.abs()) / k);
    } else {
      return ratio * ((pow(x, 3) * (k - 1) + x) / k);
    }
  }
}
