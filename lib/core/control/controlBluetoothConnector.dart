import 'dart:async';

import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:jetter/core/control/controlStore.dart';
import 'package:remote_control_communication/protocol/v1-control-encoder.dart';

class ControlBluetoothConnector {
  static ControlBluetoothConnector _connector =
      ControlBluetoothConnector._privateConstructor();

  static ControlBluetoothConnector get instance {
    return _connector;
  }

  Timer updateTimer;
  ControlProtocolEncoderV1 _encoder;

  ControlBluetoothConnector._privateConstructor()
      : _encoder = ControlProtocolEncoderV1() {
    updateTimer = Timer.periodic(Duration(milliseconds: 8), _timerCallback);
    Observer(
      builder: (_) {
        ControlStore.instance.channelValues.forEach((key, _) {
          _encoder.initControlInput(key);
          _encoder.initControlOutput(key);
        });
      },
    );
  }

  void _timerCallback(Timer timer) {
    ControlStore.instance.updateChannelValues();
//    print(_encoder.inputValues(ControlStore.instance.channelValues));
  }
}
