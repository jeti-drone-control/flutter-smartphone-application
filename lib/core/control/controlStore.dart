import 'dart:core';

import 'package:f_logs/model/flog/log_level.dart';
import 'package:jetter/core/connectivity/bluetoothStore.dart';
import 'package:jetter/core/control/channelAssignmentConfig.dart';
import 'package:jetter/core/control/controlAction.dart';
import 'package:jetter/core/control/controlBluetoothConnector.dart';
import 'package:jetter/core/control/controlDecoder.dart';
import 'package:jetter/core/control/controlDecoderDelegate.dart';
import 'package:jetter/core/control/controlEncoder.dart';
import 'package:jetter/core/control/transferFunction.dart';
import 'package:jetter/utils/logger.dart';
import 'package:mobx/mobx.dart';

import 'axis.dart';

part 'controlStore.g.dart';

class ControlStore extends _ControlStore with _$ControlStore {
  static final ControlStore _controlStore = ControlStore._privateConstructor();

  static ControlStore get instance {
    return _controlStore;
  }

  ControlStore._privateConstructor() : super();
}

abstract class _ControlStore extends ControlDecoderDelegate with Store {
  @observable
  bool isControlActive;

  @observable
  ObservableMap<ControlAxis, double> axisValues = ObservableMap();

  @observable
  ObservableMap<int, double> channelValues = ObservableMap();

  @observable
  ObservableMap<ControlAxis, Map<int, ChannelAssignmentConfig>>
      channelAssignments = ObservableMap();

  ControlDecoder _decoder;
  ControlEncoder _encoder;

  ControlAction _currentMessage;

  _ControlStore()
      : isControlActive = false,
        _encoder = ControlEncoder() {
    _decoder = ControlDecoder(this);
    BluetoothStore.instance.frameDecoder.registerPayloadDecoder(_decoder);

    axisValues[ControlAxis.LEFT_JOYSTICK_HORIZONTAL_AXIS] = 0;
    axisValues[ControlAxis.LEFT_JOYSTICK_VERTICAL_AXIS] = 0;
    axisValues[ControlAxis.RIGHT_JOYSTICK_HORIZONTAL_AXIS] = 0;
    axisValues[ControlAxis.RIGHT_JOYSTICK_VERTICAL_AXIS] = 0;

    ControlBluetoothConnector.instance;
  }

  @action
  void setControlState(bool isActive) {
    this.isControlActive = isActive;
  }

  void changeControlState(bool activate) {
    // if (_currentMessage != null) {
    //   log("[ControlStore] Failed to change control actiation state",
    //       LogLevel.ERROR);
    //   return;
    // }

    _currentMessage =
        activate ? ControlAction.ACTIVATE : ControlAction.DEACTIVATE;

    BluetoothStore.instance.sendBuffer(activate
        ? _encoder.encodeControlActivation()
        : _encoder.encodeControlDeactivation());
  }

  @action
  void addChannelAssignment(
      ControlAxis axis, int affectedChannel, ChannelAssignmentConfig config) {
    if (channelAssignments[axis] == null) {
      channelAssignments[axis] = Map();
    }
    channelAssignments[axis][affectedChannel] = config;
    channelValues[affectedChannel] = 0;
  }

  @action
  void updateAxisValue(ControlAxis axis, double position) {
    axisValues[axis] = position;
  }

  @action
  void updateChannelValues() {
    axisValues.forEach((axis, position) {
      if (channelAssignments.containsKey(axis)) {
        final assignmentConfigs = channelAssignments[axis];
        for (final affectedChannel in assignmentConfigs.keys) {
          if (channelValues.containsKey(affectedChannel)) {
            channelValues[affectedChannel] = transferFunction(
                    position / 100,
                    assignmentConfigs[affectedChannel].slope / 100,
                    assignmentConfigs[affectedChannel].exponential) *
                100;
          }
        }
      }
    });
  }

  void sendControlChannelValues() {
    log("[ControlStore] send control channel values", LogLevel.DEBUG);
    Map<int, double> channels = Map.from(channelValues);
    BluetoothStore.instance.sendBuffer(_encoder.encodeChannelValues(channels));
  }

  @override
  receivedAck() {
    log("[ControlStore] received ACK", LogLevel.DEBUG);
    switch (_currentMessage) {
      case ControlAction.ACTIVATE:
        setControlState(true);
        break;
      case ControlAction.DEACTIVATE:
        setControlState(false);
        break;
    }
    _currentMessage = null;
  }

  @override
  receivedNack() {
    log("[ControlStore] received NACK", LogLevel.DEBUG);
  }

  void reset() {
    _currentMessage = null;
    isControlActive = false;
  }
}
