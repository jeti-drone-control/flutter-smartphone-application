import 'package:jetter/core/protocol-utils/crc.dart';

class Rfc1662Encoder {
  Rfc1662Encoder();

  int calculateCrc(List<int> messageFrame) {
    int crc = 0;
    messageFrame.forEach((element) {
      crc = updateCrc(crc, element);
    });
    return crc;
  }

  List<int> encodeFrame(int protocolId, List<int> framePayload) {
    // Escape
    List<int> escapedPayload = List.from(framePayload);
    int index = 0;
    int escapedCounter = 0;
    do {
      index = framePayload.indexWhere(
          (element) => [0x7E, 0x7D].contains(element), index);
      if (index != -1) {
        escapedPayload[index + escapedCounter++] = 0x7D;
        escapedPayload.insert(
            index + escapedCounter, framePayload[index] & ~0x20);
      }
    } while (index++ != -1 && index < framePayload.length);

    escapedPayload.insert(0, escapedPayload.length);

    escapedPayload.insert(escapedPayload.length, calculateCrc(escapedPayload));
    escapedPayload.insertAll(0, [126, protocolId]);

    return escapedPayload;
  }
}
