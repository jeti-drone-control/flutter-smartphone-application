import 'package:jetter/core/protocol-utils/abstract_payload_decoder.dart';
import 'package:jetter/core/protocol-utils/crc.dart';
import 'package:jetter/core/protocol-utils/protocolConstants.dart';
import 'package:jetter/core/rfc1662-protocol/frameState.dart';
import 'package:jetter/utils/logger.dart';
import 'package:f_logs/f_logs.dart';

class Rfc1662Decoder {
  final Map<int, PayloadDecoder> _payloadDecoders;
  FrameState _frameState;
  List<int> _buffer;
  int _payloadLength;
  int _payloadCounter;
  int _payloadIdentifier;
  int _crcValue;

  Rfc1662Decoder()
      : _payloadDecoders = Map(),
        _frameState = FrameState.IDLE,
        _buffer = [];

  void registerPayloadDecoder(PayloadDecoder payloadDecoder) {
    this
        ._payloadDecoders
        .putIfAbsent(payloadDecoder.payloadIdentifier, () => payloadDecoder);
  }

  void _clear() {
    log('[RFC1662Decoder] Clear decoder', LogLevel.DEBUG);
    _crcValue = 0;
    _payloadCounter = 0;
    _payloadLength = 0;
    _payloadIdentifier = -1;
    _buffer.clear();
  }

  void _notifyPayloadDecoder() {
    if (_payloadIdentifier == -1) {
      log("[RFC1662Decoder] No payloadId available", LogLevel.DEBUG);
      return;
    }

    if (!_payloadDecoders.containsKey(_payloadIdentifier)) {
      log("[RFC1662Decoder] Payload decoder for identifier $_payloadIdentifier is not available",
          LogLevel.DEBUG);
      return;
    }

    log("[RFC1662Decoder] Notify payload decoder with identifier $_payloadIdentifier",
        LogLevel.DEBUG);
    List<int> bufferCopy = List.from(_buffer);
    _payloadDecoders[_payloadIdentifier].decodePayload(bufferCopy);
  }

  void _handleFrameState(int value) {
    if (value == frameIndicator) {
      if (value == frameIndicator) {
        _clear();
        log('[RFC1662Decoder] New frame detected', LogLevel.DEBUG);
        _frameState = FrameState.FRAME_START;
        return;
      }
    }

    switch (_frameState) {
      case FrameState.FRAME_START:
        log('[RFC1662Decoder] Detected subprotocol $value', LogLevel.DEBUG);
        _payloadIdentifier = value;
        _frameState = FrameState.PROTOCOL_IDENTIFIER;
        break;
      case FrameState.PROTOCOL_IDENTIFIER:
        log('[RFC1662Decoder] Detected payload length $value', LogLevel.DEBUG);
        _payloadLength = value;
        _frameState = FrameState.PAYLOAD_LENGTH;
        break;
      case FrameState.PAYLOAD_LENGTH:
        _frameState = FrameState.FRAME_CONTENT;
        break;
      case FrameState.FRAME_CONTENT:
        if (_payloadCounter == _payloadLength) {
          log('Reached payload length. Detected CRC', LogLevel.DEBUG);
          _frameState = FrameState.PAYLOAD_COMPLETE;
          break;
        }

        if (value == escapeCharacter) {
          log('Detected escaping indicator $value', LogLevel.DEBUG);
        }
        break;
      case FrameState.ESCAPE_INDICATOR:
        log('Detected escaped value $value', LogLevel.DEBUG);
        _frameState = FrameState.ESCAPED_CHARACTER;
        break;
      // TODO notify callback
      default:
        break;
    }

    if (_frameState == FrameState.PAYLOAD_COMPLETE) {
      if (value == _crcValue) {
        log('[RFC1662Decoder] CRC valid', LogLevel.DEBUG);
        _notifyPayloadDecoder();
      } else {
        log('[RFC1662Decoder] CRC invalid: $value, required: $_crcValue',
            LogLevel.DEBUG);
      }

      _frameState = FrameState.IDLE;
      _clear();
    }
  }

  void _handleValue(int value) {
    switch (this._frameState) {
      case FrameState.PAYLOAD_LENGTH:
        _crcValue = updateCrc(_crcValue, value);
        break;
      case FrameState.FRAME_CONTENT:
        _crcValue = updateCrc(_crcValue, value);
        _buffer.insert(
            _payloadCounter > _buffer.length ? _payloadLength : _buffer.length,
            value);
        _payloadCounter++;
        break;
      case FrameState.ESCAPE_INDICATOR:
        _crcValue = updateCrc(_crcValue, value);
        _payloadCounter++;
        break;
      case FrameState.ESCAPED_CHARACTER:
        _crcValue = updateCrc(_crcValue, value);
        // Deescape value
        _payloadCounter++;
        break;
      default:
        break;
    }
  }

  void updateBuffer(List<int> valueBuffer) {
    valueBuffer.forEach((element) {
      _handleFrameState(element);
      _handleValue(element);
    });
  }
}
