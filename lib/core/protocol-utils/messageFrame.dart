class MessageFrame {
  final List<int> _messageBuffer;

  MessageFrame(List<int> payload) : _messageBuffer = List(payload.length);

  List<int> get buffered => _messageBuffer;
}
