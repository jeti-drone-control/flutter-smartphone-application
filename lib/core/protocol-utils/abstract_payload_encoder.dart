import 'package:jetter/core/rfc1662-protocol/rfc1662-encoder.dart';

abstract class PayloadEncoder {
  Rfc1662Encoder _frameEncoder;

  int get protocolIdentifier;

  PayloadEncoder() : _frameEncoder = Rfc1662Encoder();

  encodeFrame(List<int> payload) =>
      _frameEncoder.encodeFrame(protocolIdentifier, payload);
}
