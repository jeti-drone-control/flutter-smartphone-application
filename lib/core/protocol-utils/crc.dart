import 'package:f_logs/f_logs.dart' as FLog;
import 'package:jetter/utils/logger.dart';

int updateCrc(int crcSeed, int value) {
  int crc = value ^ crcSeed;
  for (var i = 0; i < 8; i++) {
    crc = (((crc & 0x80) > 0) ? 0x07 ^ (crc << 1) : (crc << 1)) & 0xFF;
  }
  return crc;
}
