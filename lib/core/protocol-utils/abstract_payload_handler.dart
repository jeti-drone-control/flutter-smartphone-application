abstract class PayloadHandler<T> {
  void onPayloadDecoded(T payload);
}
