abstract class Transmitter {
  void sendBuffer(List<int> buffer);
}
