import 'package:f_logs/f_logs.dart';
import 'package:jetter/core/protocol-utils/abstract_payload_handler.dart';
import 'package:jetter/utils/logger.dart';

abstract class PayloadDecoder {
  final int _payloadIdentifier;
  int get payloadIdentifier => _payloadIdentifier;

  PayloadDecoder(this._payloadIdentifier);

  void handleContent(List<int> buffer);

  void decodePayload(List<int> buffer) async {
    if (buffer.length == 0) {
      log("Buffer cannot be decoded. No values available", LogLevel.DEBUG);
      return;
    }

    handleContent(buffer);
  }
}
