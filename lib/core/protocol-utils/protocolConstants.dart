final int frameIndicator = 0x7E;
final int escapeCharacter = 0x7D;

final int connectivityProtocolIdentifier = 1;

final int telemetryProtocolIdentifier = 2;

final int controlProtocolIdentifier = 3;
final int controlProtocolActivate = 1;
final int controlProtocolDectivate = 2;
final int controlProtocolValues = 10;
