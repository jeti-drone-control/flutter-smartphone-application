import 'package:jetter/core/protocol-utils/abstract_payload_encoder.dart';
import 'package:jetter/core/protocol-utils/protocolConstants.dart';
import 'package:jetter/core/rfc1662-protocol/rfc1662-encoder.dart';

class ConnectivityEncoder extends PayloadEncoder {
  ConnectivityEncoder() : super();

  @override
  int get protocolIdentifier => connectivityProtocolIdentifier;

  List<int> encodePing() {
    return encodeFrame([1]);
  }

  List<int> encodeSystemReset() {
    return encodeFrame([10]);
  }
}
