import 'dart:async';

import 'package:f_logs/f_logs.dart';
import 'package:f_logs/model/flog/log_level.dart';
import 'package:jetter/core/connectivity/bluetooth-status.dart';
import 'package:jetter/core/connectivity/bluetoothStore.dart';
import 'package:jetter/core/connectivity/connectivityDecoder.dart';
import 'package:jetter/core/connectivity/connectivityDecoderDelegate.dart';
import 'package:jetter/core/connectivity/connectivityEncoder.dart';
import 'package:jetter/core/control/controlStore.dart';
import 'package:jetter/core/telemetry/telemetryStore.dart';
import 'package:jetter/utils/logger.dart';
import 'package:mobx/mobx.dart';

part 'connectivityStore.g.dart';

class ConnectivityStore extends _ConnectivityStore with _$ConnectivityStore {
  static final ConnectivityStore _connectivityStore =
      ConnectivityStore._privateConstructor();

  static ConnectivityStore get instance {
    return _connectivityStore;
  }

  ConnectivityStore._privateConstructor() : super();
}

abstract class _ConnectivityStore
    with Store
    implements ConnectivityDecoderDelegage {
  @observable
  bool _isAwaitingPong;

  @observable
  bool pingPongEnabled;

  @observable
  bool _isOnline;

  @computed
  bool get isOnline =>
      BluetoothStore.instance.status == BluetoothStatus.CONNECTED &&
      pingPongEnabled;

  Timer _pingPongTimer;

  ConnectivityDecoder _decoder;

  final ConnectivityEncoder _encoder;

  _ConnectivityStore()
      : _isAwaitingPong = false,
        pingPongEnabled = false,
        _isOnline = false,
        _encoder = ConnectivityEncoder() {
    _decoder = ConnectivityDecoder(this);
    BluetoothStore.instance.frameDecoder.registerPayloadDecoder(_decoder);
    BluetoothStore.instance.bluetoothStatus.listen((bluetoothStatus) {
      if (bluetoothStatus != BluetoothStatus.CONNECTED) {
        setOffline();
      }
      log("[ConnectivityStore] got bluetooth status update $bluetoothStatus",
          LogLevel.DEBUG);
    });
  }

  @action
  void setOffline() {
    _isOnline = false;
  }

  void fireTimer(Timer timer) {
    if (!_isAwaitingPong) {
      // if (TelemetryStore.instance.isCommunicationActive) {
      //   log("[ConnectivityStore] Telemetry communication active, not sending ping",
      //       LogLevel.DEBUG);
      //   return;
      // }
      sendPing();
      return;
    }

    stopPingPong();
  }

  @action
  void startPingPong() {
    if (_pingPongTimer != null) {
      log("[ConnectivityStore] Ping pong timer already running",
          LogLevel.DEBUG);
      return;
    }

    log("[ConnectivityStore] Start ping pong", LogLevel.DEBUG);

    _pingPongTimer = Timer.periodic(Duration(milliseconds: 500), fireTimer);
    _isAwaitingPong = false;
    pingPongEnabled = true;
  }

  @action
  void stopPingPong() {
    log("[ConnectivityStore] Stop ping pong", LogLevel.DEBUG);
    if (_pingPongTimer == null) {
      log("[ConnectivityStore] Failed to reset ping pong timer",
          LogLevel.ERROR);
      return;
    }
    log("[ConnectivityStore] Reset ping pong timer", LogLevel.DEBUG);
    _pingPongTimer.cancel();
    pingPongEnabled = false;
    _pingPongTimer = null;
  }

  @action
  void sendPing() {
    log("[Connectivity Store] Send ping", LogLevel.DEBUG);
    List<int> pingMessage = _encoder.encodePing();
    BluetoothStore.instance.sendBuffer(pingMessage);
    _isAwaitingPong = true;
  }

  @action
  void pong() {
    log("[Connectivity Store] Received pong", LogLevel.DEBUG);
    print("${DateTime.now()} received");
    _isAwaitingPong = false;
    _isOnline = true;

    ControlStore.instance.sendControlChannelValues();
    TelemetryStore.instance.timedTelemetryAction();
  }

  @override
  void receivedPingResponse() {
    pong();
  }

  void receivedResetRequest() {
    log("[ConnectivityStore] Ble device requests reset", LogLevel.DEBUG);
    reset();
    TelemetryStore.instance.reset();
  }

  void resetBleDevice() {
    log("[ConnectivityStore] Reset", LogLevel.DEBUG);
    BluetoothStore.instance.sendBuffer(_encoder.encodeSystemReset());
  }

  @action
  void reset() {
    this._isAwaitingPong = false;
    this._isOnline = false;
    this.pingPongEnabled = false;
  }
}
