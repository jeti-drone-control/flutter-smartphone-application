import 'package:f_logs/f_logs.dart';
import 'package:jetter/core/connectivity/connectivityDecoderDelegate.dart';
import 'package:jetter/core/protocol-utils/abstract_payload_decoder.dart';
import 'package:jetter/core/protocol-utils/abstract_payload_handler.dart';
import 'package:jetter/core/protocol-utils/protocolConstants.dart';
import 'package:jetter/utils/logger.dart';

class ConnectivityDecoder extends PayloadDecoder {
  ConnectivityDecoderDelegage _delegate;

  ConnectivityDecoder(this._delegate) : super(connectivityProtocolIdentifier);

  @override
  void handleContent(List<int> buffer) {
    switch (buffer[0]) {
      case 1:
        // Should never happen since 1 is the ping sent by the device
        break;
      case 2:
        log("Received pong", LogLevel.DEBUG);
        _delegate.receivedPingResponse();
        // Pong
        break;
      case 10:
        _delegate.receivedResetRequest();
    }
  }
}
