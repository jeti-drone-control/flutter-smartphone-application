abstract class ConnectivityDecoderDelegage {
  void receivedPingResponse();
  void receivedResetRequest();
}
