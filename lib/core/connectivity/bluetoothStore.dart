import 'dart:async';

import 'package:f_logs/f_logs.dart' as Flog;
import 'package:flutter/services.dart';
import 'package:jetter/core/connectivity/connectivityStore.dart';
import 'package:jetter/core/constants/errorCodes.dart';
import 'package:jetter/core/control/controlStore.dart';
import 'package:jetter/core/rfc1662-protocol/rfc1662-decoder.dart';
import 'package:jetter/core/telemetry/telemetryStore.dart';
import 'package:jetter/utils/logger.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter_blue/flutter_blue.dart';

import 'package:jetter/core/protocol-utils/abstract_payload_decoder.dart';
import 'package:jetter/core/protocol-utils/abstract_transmitter.dart';
import 'package:jetter/core/connectivity/bluetooth-status.dart';

part 'bluetoothStore.g.dart';

const scanTimeoutSeconds = 30;

class BluetoothStore extends _BluetoothStore with _$BluetoothStore {
  static final BluetoothStore _bluetoothStore =
      BluetoothStore._privateConstructor();

  static BluetoothStore get instance {
    return _bluetoothStore;
  }

  BluetoothStore._privateConstructor() : super();
}

abstract class _BluetoothStore with Store implements Transmitter {
  FlutterBlue flutterBlue;

  @observable
  BluetoothStatus status;

  @computed
  BluetoothStatus get statusObservable => status;

  @observable
  ObservableMap devices = ObservableMap();

  @observable
  BluetoothDevice connectedDevice;

  BluetoothCharacteristic characteristic;

  final Rfc1662Decoder _frameDecoder;

  Rfc1662Decoder get frameDecoder => _frameDecoder;

  StreamController<BluetoothStatus> _bluetoothStatusStream =
      StreamController<BluetoothStatus>();

  _BluetoothStore() : _frameDecoder = Rfc1662Decoder() {
    log("[BluetoothStore] Initialize BluetoothStore instance",
        Flog.LogLevel.DEBUG);
    try {
      flutterBlue = false ? null : FlutterBlue.instance;
      flutterBlue.state.listen(_updateBluetoothDeviceStatus);
      flutterBlue.isScanning.listen(_scanStatusUpdate);
      log("[BluetoothStore] Successfully initialized FlutterBlue",
          Flog.LogLevel.DEBUG);
    } catch (error) {
      this.status = BluetoothStatus.UNAVAILABLE;
      log("[BluetoothStore] FlutterBlue Initialization failed",
          Flog.LogLevel.DEBUG);
    }
  }

  Stream<BluetoothStatus> get bluetoothStatus => _bluetoothStatusStream.stream;

  bool _isCharacteristicWriteable(CharacteristicProperties properties) {
    return properties.write || properties.writeWithoutResponse;
  }

  BluetoothCharacteristic _filterCharacteristic(
      List<BluetoothService> services) {
    log("[BluetoothStore] Filter characteristic", Flog.LogLevel.DEBUG);
    var readWriteCharacteristic;
    services
        .takeWhile((_) => readWriteCharacteristic == null)
        .forEach((service) {
      service.characteristics
          .takeWhile((_) => readWriteCharacteristic == null)
          .forEach((characteristic) {
        final properties = characteristic.properties;
        log("[BluetoothStore] Found charakteristic ${characteristic.serviceUuid}  ${characteristic.descriptors} with properties ${characteristic.properties}",
            Flog.LogLevel.DEBUG);
        if (properties.read &&
            properties.notify &&
            _isCharacteristicWriteable(properties)) {
          readWriteCharacteristic = characteristic;
        }
      });
    });

    if (readWriteCharacteristic == null) {
      log("[BluetoothStore] No matching characteristic found",
          Flog.LogLevel.DEBUG);
      throw Error();
    }
    return readWriteCharacteristic;
  }

  @action
  void _setBluetoothStatus(BluetoothStatus bluetoothStatus) {
    log("[BluetoothStore] Bluetooth status changed: ${bluetoothStatus.toString()}",
        Flog.LogLevel.DEBUG);
    this.status = bluetoothStatus;
    _bluetoothStatusStream.sink.add(bluetoothStatus);
  }

  @action
  void addDeviceToMap(String id, BluetoothDevice device) {
    this.devices[id] = device;
  }

  bool isDeviceRelevant(String id, BluetoothDevice device) {
    return device.name.length > 0 && !this.devices.containsKey(id);
  }

  void _addDevice(BluetoothDevice device) {
    final deviceId = device.id.toString();
    if (isDeviceRelevant(deviceId, device)) {
      log("[BluetoothStore] Relevant device discovered: ${device.name}",
          Flog.LogLevel.DEBUG);
      addDeviceToMap(deviceId, device);
    }
  }

  void _newDeviceDiscovered(ScanResult scanResult) {
    final device = scanResult.device;
    _addDevice(device);
  }

  @action
  void _updateBluetoothDeviceStatus(BluetoothState state) {
    log("[BluetoothStore] Update bluetooth status: ${state.toString()}",
        Flog.LogLevel.DEBUG);
    this._setBluetoothStatus(state == BluetoothState.on
        ? this.status = BluetoothStatus.IDLE
        : this.status = BluetoothStatus.OFF);
  }

  @action
  Future startScan() async {
    if (this.status != BluetoothStatus.IDLE) throw Error();
    log("[BluetoothStore] Start Bluetooth scan", Flog.LogLevel.DEBUG);
    devices.clear();
    final List<BluetoothDevice> connectedDevices =
        await flutterBlue.connectedDevices;
    connectedDevices.forEach(_addDevice);
    flutterBlue
        .scan(timeout: Duration(seconds: 30))
        .listen(_newDeviceDiscovered);
    this._setBluetoothStatus(BluetoothStatus.SCANNING);
  }

  Future stopScan() async {
    log("[BluetoothStore] Stop Bluetooth scan", Flog.LogLevel.DEBUG);
    flutterBlue.stopScan().then((_) {
      this._setBluetoothStatus(BluetoothStatus.IDLE);
      log("[BluetoothStore] Bluetooth scan stopped", Flog.LogLevel.DEBUG);
    });
  }

  @action
  void _scanStatusUpdate(bool isScanning) {
    if (this.status == BluetoothStatus.CONNECTING) return;
    this._setBluetoothStatus(
        isScanning ? BluetoothStatus.SCANNING : BluetoothStatus.IDLE);
  }

  void valueUpdate(List<int> buffer) {
    log("[BluetoothStore] Received value update ${buffer.toString()}",
        Flog.LogLevel.DEBUG);
    this._frameDecoder.updateBuffer(buffer);
  }

  void _selectCharacteristic(BluetoothDevice device) async {
    try {
      List<BluetoothService> services = await device.discoverServices();
      log("[BluetoothStore] Found services: $services", Flog.LogLevel.DEBUG);
      final characteristic = _filterCharacteristic(services);
      if (characteristic == null) {
        log("[BluetoothStore] Did not found a matching characteristic $services",
            Flog.LogLevel.DEBUG);
        return;
      }

      this.characteristic = characteristic;
      this.connectedDevice = device;

      log("[BluetoothStore] Set notification", Flog.LogLevel.DEBUG);
      await this.characteristic.setNotifyValue(true);
      this.characteristic.value.listen(valueUpdate);
      log("[BluetoothStore] Device is listening", Flog.LogLevel.DEBUG);
    } catch (e) {
      this.connectedDevice.disconnect();
      log("[BluetoothStore] Could not connect to device $e",
          Flog.LogLevel.ERROR);
    } catch (e) {
      log("[BluetoothStore] Disconnect failed $e", Flog.LogLevel.ERROR);
    }

    this.devices.clear();
    this._setBluetoothStatus(BluetoothStatus.CONNECTED);
  }

  @action
  void _deviceConnected(BluetoothDevice device) {
    log("[BluetoothStore] Device connected", Flog.LogLevel.DEBUG);
    try {
      _selectCharacteristic(device);
    } catch (e) {
      log("[BluetoothStore] Failed to select characteristic $e. Reset status to idle.",
          Flog.LogLevel.ERROR);
      _setBluetoothStatus(BluetoothStatus.IDLE);
    }
  }

  @action
  void _connectionFailed(BluetoothDevice device) {
    log("[BluetoothStore] Connection failed with device ${device.name}",
        Flog.LogLevel.DEBUG);
    this.connectedDevice = null;
    this._setBluetoothStatus(BluetoothStatus.IDLE);
  }

  void connectDevice(deviceSsid) async {
    log("[BluetoothStore] Should connect device $deviceSsid",
        Flog.LogLevel.DEBUG);
    if (this.connectedDevice != null ||
        [BluetoothStatus.CONNECTED, BluetoothStatus.CONNECTING]
            .contains(this.status)) {
      // TODO need timeout
      throw Error();
    }

    if (!devices.containsKey(deviceSsid)) throw Error();

    if (this.status == BluetoothStatus.SCANNING) {
      await flutterBlue.stopScan();
    }

    // this.setBluetoothStatus(BluetoothStatus.CONNECTING);
    final BluetoothDevice device = devices[deviceSsid];

    device.state.listen((event) {
      log("[BluetoothStore] Device state update $event", Flog.LogLevel.DEBUG);
    });

    try {
      _setBluetoothStatus(BluetoothStatus.CONNECTING);
      await device.connect();
      _deviceConnected(device);
    } catch (e) {
      log("[BluetoothStore] Could not connet device $e", Flog.LogLevel.ERROR);
      if (e is PlatformException &&
          (e as PlatformException).code == bleDeviceAlreadyConnected) {
        log("[BluetoothStore] Device has been connected already",
            Flog.LogLevel.DEBUG);
        _deviceConnected(device);
      } else {
        _setBluetoothStatus(BluetoothStatus.IDLE);
      }
    }
  }

  @action
  void _deviceDisconnected(_) {
    log("[BluetoothStore] Device disconnected", Flog.LogLevel.DEBUG);
    this.characteristic = null;
    this.connectedDevice = null;
    this._setBluetoothStatus(BluetoothStatus.IDLE);
  }

  void disconnect() async {
    log("[BluetoothStore] Disconnect device ${this.connectedDevice.name}",
        Flog.LogLevel.DEBUG);
    if (this.status != BluetoothStatus.CONNECTED ||
        this.connectedDevice == null) {
      throw Error();
    }
    await this.connectedDevice.disconnect();

    // If this does not work, check other apps
    this._deviceDisconnected(null);
  }

  @override
  void sendBuffer(List<int> buffer) {
    log("[BluetoothStore] Send buffer ${buffer.toString()}",
        Flog.LogLevel.DEBUG);
    if (this.characteristic == null) {
      log("[BluetoothStore] No characteristic available for sending buffer",
          Flog.LogLevel.ERROR);
    }

    try {
      this.characteristic.write(buffer);
    } on PlatformException catch (err) {
      log("[BluetoothStore] Failed to write buffer: $err", Flog.LogLevel.ERROR);
    } catch (e) {
      log("[BluetoothStore] Failed to write buffer: $e", Flog.LogLevel.ERROR);
    }
  }

  set payloadDecoder(PayloadDecoder payloadDecoder) {
    this._frameDecoder.registerPayloadDecoder(payloadDecoder);
  }

  void readValue() async {
    if (characteristic == null) {
      log("[BluetoothStore] No characteristic available. Cannot read from the device",
          Flog.LogLevel.ERROR);
      return;
    }

    List<int> lastValue = await characteristic.read();
    log("[BluetoothStore] Read values ${lastValue.toString()}",
        Flog.LogLevel.DEBUG);
  }

  Future<void> resetSystems() async {
    log("[BluetoothStore] Reset systems", Flog.LogLevel.DEBUG);
    ConnectivityStore.instance.resetBleDevice();
    ConnectivityStore.instance.reset();
    TelemetryStore.instance.reset();
    ControlStore.instance.reset();
    try {
      disconnect();
    } catch (e) {
      log("[BluetoothStore] Failed to disconnect $e", Flog.LogLevel.ERROR);
    }
    _deviceDisconnected(null);
  }
}
