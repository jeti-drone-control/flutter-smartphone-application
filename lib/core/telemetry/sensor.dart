import 'package:f_logs/model/flog/log_level.dart';
import 'package:jetter/core/telemetry/sensorMeasurement.dart';
import 'package:jetter/utils/logger.dart';
import 'package:mobx/mobx.dart';

part 'sensor.g.dart';

class Sensor extends _Sensor with _$Sensor {
  Sensor(int manufacturerId, int deviceId, String title, String subtitle)
      : super(manufacturerId, deviceId, title, subtitle);

  bool operator ==(sensor) =>
      sensor is Sensor &&
      sensor.manufacturerId == manufacturerId &&
      sensor.deviceId == deviceId;
}

abstract class _Sensor with Store {
  final int deviceId;
  final int manufacturerId;
  final String title;
  final String subtitle;

  @observable
  ObservableList<SensorMeasurement> measurements =
      ObservableList<SensorMeasurement>();

  _Sensor(this.manufacturerId, this.deviceId, this.title, this.subtitle);

  @action
  void addMeasurement(SensorMeasurement measurement) {
    if (measurements.contains(measurement)) {
      return;
    }
    measurements.add(measurement);
    // measurements.sort();
  }

  SensorMeasurement getSensorMeasurement(int sensorId) {
    Iterable<SensorMeasurement> it =
        measurements.where((measurement) => measurement.sensorId == sensorId);
    if (it.length != 1) {
      log("[SensorMeasurement] Failed ot get sensor measurement $sensorId",
          LogLevel.DEBUG);
      return null;
    }

    return it.first;
  }
}
