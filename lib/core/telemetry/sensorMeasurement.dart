import 'package:jetter/core/telemetry/sensor.dart';
import 'package:jetter/core/telemetry/sensorValue.dart';
import 'package:jetter/core/telemetry/telemetryStore.dart';
import 'package:mobx/mobx.dart';

part 'sensorMeasurement.g.dart';

class SensorMeasurement extends _SensorMeasurement with _$SensorMeasurement {
  SensorMeasurement(
      Sensor sensor, int sensorId, String description, String unit)
      : super(sensor, sensorId, description, unit);

  bool operator ==(o) => o is SensorMeasurement && o.sensorId == sensorId;
}

abstract class _SensorMeasurement with Store {
  final int sensorId;
  final String description;
  final String unit;
  final Sensor _sensor;

  @observable
  SensorValue sensorValue;

  int get manufacturerId => _sensor.manufacturerId;
  int get deviceId => _sensor.deviceId;

  @observable
  bool isSubscribed;

  _SensorMeasurement(this._sensor, this.sensorId, this.description, this.unit)
      : isSubscribed = false;

  @action
  void setSensorValue(SensorValue sensorValue) {
    this.sensorValue = sensorValue;
  }

  @action
  void subscribed(int subscriptionId) {
    isSubscribed = true;
  }

  @action
  void unsubscribed() {
    isSubscribed = false;
  }

  Future<void> unsubscribe() async {
    TelemetryStore.instance.unsubscribeSensor(this);
  }

  Future<void> subscribe() async {
    TelemetryStore.instance.subscribeSensor(this);
  }
}
