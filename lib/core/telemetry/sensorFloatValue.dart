import 'package:jetter/core/telemetry/sensorValue.dart';
import 'package:jetter/core/telemetry/sensorValueType.dart';
import 'package:mobx/mobx.dart';

part 'sensorFloatValue.g.dart';

class SensorFloatValue extends _SensorFloatValue with _$SensorFloatValue {
  SensorFloatValue(double value) : super(value);
}

abstract class _SensorFloatValue extends SensorValue with Store {
  @observable
  double value;

  _SensorFloatValue(this.value) : super(SensorValueType.FLOAT);

  @action
  void setValue(double valueUpdate) {
    value = valueUpdate;
  }
}
