import 'dart:ffi';
import 'dart:typed_data';

import 'package:f_logs/model/flog/log_level.dart';
import 'package:flutter/services.dart';
import 'package:jetter/core/connectivity/bluetoothStore.dart';
import 'package:jetter/core/protocol-utils/abstract_payload_decoder.dart';
import 'package:jetter/core/protocol-utils/protocolConstants.dart';
import 'package:jetter/core/telemetry/sensor.dart';
import 'package:jetter/core/telemetry/telemetryDecoderDelegate.dart';
import 'package:jetter/core/telemetry/telemetryStore.dart';
import 'package:jetter/utils/logger.dart';

class TelemetryDecoder extends PayloadDecoder {
  TelemetryDecoderDelegate _delegate;
  TelemetryDecoder(this._delegate) : super(telemetryProtocolIdentifier) {
    BluetoothStore.instance.frameDecoder.registerPayloadDecoder(this);
  }

  int decodeUInt16(int msb, int lsb) {
    log('[TelemetryDecoder] Decode uint16 value $msb (msb), $lsb (lsb): ${(msb << 8) + lsb}',
        LogLevel.DEBUG);
    return (msb << 8) + lsb;
  }

  void decoderSensorDescription(List<int> buffer) {
    log('[TelemetryDecoder] Decode sensor description', LogLevel.DEBUG);
    final int manufacturerId = decodeUInt16(buffer[1], buffer[2]);
    final int deviceId = decodeUInt16(buffer[3], buffer[4]);
    final int sensorId = buffer[5];
    final String payload = String.fromCharCodes(buffer.sublist(6));
    log('[TelemetryDecoder] Description payload $payload', LogLevel.DEBUG);
    final List<String> content = payload.split('#');
    log('[TelemetryDecoder] Payload content $content', LogLevel.DEBUG);

    log('[TelemetryDecoder] Identified sensor $manufacturerId, $deviceId, $sensorId',
        LogLevel.DEBUG);
    if (content.length != 2) {
      log('[TelemetryDecoder] Failed to decode sensor payload $content',
          LogLevel.ERROR);
      return;
    }

    final String title = content[0];
    final String subtitle = content[1] != '' ? content[1] : null;

    log('[TelemetryDecoder] Name $title, unit ${subtitle == null ? "n.A." : subtitle}',
        LogLevel.DEBUG);
    // final Sensor sensor = new Sensor(manufacturerId, deviceId, title, subtitle);
    _delegate.sensorIdentified(
        manufacturerId, deviceId, sensorId, title, subtitle);
  }

  void decodeSubscriptionResponse(List<int> buffer, Function callback) {
    final int manufacturerId = decodeUInt16(buffer[1], buffer[2]);
    final int deviceId = decodeUInt16(buffer[3], buffer[4]);
    final int sensorId = buffer[5];
    final int subscriptionId = buffer[6];
    callback(manufacturerId, deviceId, sensorId, subscriptionId);
  }

  void decodeSubscriptionConfirmation(List<int> buffer) {
    log('[TelemetryDecoder] receive subscription confirmation', LogLevel.DEBUG);
    decodeSubscriptionResponse(buffer, _delegate.sensorSubscriptionConfirmed);
  }

  void decodeSubscriptionCancellation(List<int> buffer) {
    log('[TelemetryDecoder] receive subscription cancellation confirmation',
        LogLevel.DEBUG);
    decodeSubscriptionResponse(buffer, _delegate.sensorSubscriptionCancelled);
  }

  void decodeDateValue(List<int> buffer) {
    log('[TelemetryDecoder] Decode date value', LogLevel.DEBUG);
    final int subscriptionId = buffer[1];
    _delegate.updateDateSubscription(
        subscriptionId, buffer[2], buffer[3], buffer[4]);
  }

  void decodeTimeValue(List<int> buffer) {
    log('[TelemetryDecoder] Decode time value', LogLevel.DEBUG);
    final int subscriptionId = buffer[1];
    _delegate.updateTimeSubscription(subscriptionId, buffer[2], buffer[3],
        decodeUInt16(buffer[4], buffer[5]));
  }

  double getFloatValue(List<int> bytes) {
    if (bytes.length != 4) {
      log('[TelemetryDecoder] Failed to decode float value from $bytes',
          LogLevel.ERROR);
      return 0;
    }

    return ByteData.sublistView(Uint8List.fromList(bytes))
        .getFloat32(0, Endian.little);
  }

  // "[32, 1, 0, 0, 96 (x60), (64) 0x40]" -> 3.5
  void decodeFloatValue(List<int> buffer) {
    log('[TelemetryDecoder] Decode float value', LogLevel.DEBUG);
    final int subscriptionId = buffer[1];
    double value = getFloatValue(buffer.sublist(2, 6));
    _delegate.updateFloatSubscription(subscriptionId, value);
  }

  void decodeGpsValue(List<int> buffer) {
    log('[TelemetryDecoder] Decode gps value', LogLevel.DEBUG);
    final int subscriptionId = buffer[1];
  }

  @override
  void handleContent(List<int> buffer) {
    log('[TelemetryDecoder] received buffer with content type ${buffer[0]}',
        LogLevel.DEBUG);
    switch (buffer[0]) {
      case 0:
        return _delegate.receivedNack();
      case 13:
        return decoderSensorDescription(buffer);
      case 21:
        return decodeSubscriptionConfirmation(buffer);
      case 23:
        return decodeSubscriptionCancellation(buffer);
      case 30:
        return decodeDateValue(buffer);
      case 31:
        return decodeTimeValue(buffer);
      case 32:
        return decodeFloatValue(buffer);
      case 33:
        return decodeGpsValue(buffer);
      case 255:
        return _delegate.receivedAck();
    }
  }
}
