import 'package:f_logs/model/flog/log_level.dart';
import 'package:jetter/core/protocol-utils/abstract_payload_encoder.dart';
import 'package:jetter/core/protocol-utils/protocolConstants.dart';
import 'package:jetter/utils/logger.dart';

class TelemetryEncoder extends PayloadEncoder {
  TelemetryEncoder() : super();

  @override
  int get protocolIdentifier => telemetryProtocolIdentifier;

  List<int> activateTelemetyExchange() => encodeFrame([1]);

  List<int> startScan() {
    log("[TelemetryEncoder] Encode frame to start scan", LogLevel.DEBUG);
    return encodeFrame([10]);
  }

  List<int> stopScan() {
    log("[TelemetryEncoder] Encode frame to stop scan", LogLevel.DEBUG);
    return encodeFrame([11]);
  }

  List<int> pullIdentifiedSensors() {
    log("[TelemetryEncoder] Pull identified sensors", LogLevel.DEBUG);
    return encodeFrame([12]);
  }

  List<int> subscribeToSensor(
      int manufacturerId, int deviceId, int sensorId, int subscriptionId) {
    log("[TelemetryEncoder] Encode sensor subscription", LogLevel.DEBUG);
    List<int> subscriptionPayload = List<int>();
    subscriptionPayload.add(20);
    subscriptionPayload.add((manufacturerId >> 8) & 0xFF);
    subscriptionPayload.add(manufacturerId & 0xFF);
    subscriptionPayload.add((deviceId >> 8) & 0xFF);
    subscriptionPayload.add(deviceId & 0xFF);
    subscriptionPayload.add(sensorId & 0xFF);
    subscriptionPayload.add(subscriptionId & 0xFF);
    return encodeFrame(subscriptionPayload);
  }

  List<int> unsubscribeSensor(int manufacturerId, int deviceId, int sensorId) {
    log("[TelemetryEncoder] Encode sensor unsubscription", LogLevel.DEBUG);
    List<int> subscriptionPayload = List<int>();
    subscriptionPayload.add(22);
    subscriptionPayload.add((manufacturerId >> 8) & 0xFF);
    subscriptionPayload.add(manufacturerId & 0xFF);
    subscriptionPayload.add((deviceId >> 8) & 0xFF);
    subscriptionPayload.add(deviceId & 0xFF);
    subscriptionPayload.add(sensorId & 0xFF);
    return encodeFrame(subscriptionPayload);
  }

  List<int> pullActiveSubscriptions() {
    log("[TelemetryEncoder] Encode pull of active subscriptions",
        LogLevel.DEBUG);
    return encodeFrame([24]);
  }
}
