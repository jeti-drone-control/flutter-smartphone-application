import 'package:jetter/core/telemetry/sensorValueType.dart';

abstract class SensorValue {
  final SensorValueType valueType;

  SensorValue(this.valueType);
}
