import 'dart:ffi';

import 'package:jetter/core/telemetry/sensor.dart';

abstract class TelemetryDecoderDelegate {
  void receivedAck();
  void receivedNack();
  void sensorIdentified(int manufacturerId, int deviceId, int sensorId,
      String title, String subtitle);
  void sensorSubscriptionConfirmed(
      int manufacturerId, int deviceId, int sensorId, int subscriptionId);
  void sensorSubscriptionCancelled(
      int manufacturerId, int deviceId, int sensorId, int subscriptionId);
  void updateFloatSubscription(int subscriptionId, double value);
  void updateGpsSubscription(
      int subscriptionId, double position, int orientation);
  void updateDateSubscription(
      int subscriptionId, int hour, int minute, int second);
  void updateTimeSubscription(int subscriptionId, int day, int month, int year);
}
