import 'dart:async';
import 'dart:core';
import 'dart:math';

import 'package:f_logs/f_logs.dart';
import 'package:jetter/core/connectivity/bluetooth-status.dart';
import 'package:jetter/core/connectivity/bluetoothStore.dart';
import 'package:jetter/core/telemetry/sensor.dart';
import 'package:jetter/core/telemetry/sensorFloatValue.dart';
import 'package:jetter/core/telemetry/sensorMeasurement.dart';
import 'package:jetter/core/telemetry/telemetryAction.dart';
import 'package:jetter/core/telemetry/telemetryDecoder.dart';
import 'package:jetter/core/telemetry/telemetryDecoderDelegate.dart';
import 'package:jetter/core/telemetry/telemetryEncoder.dart';
import 'package:jetter/utils/logger.dart';
import 'package:mobx/mobx.dart';

part 'telemetryStore.g.dart';

class TelemetryStore extends _TelemetryStore with _$TelemetryStore {
  static final TelemetryStore _instance = TelemetryStore._privateConstructor();

  static TelemetryStore get instance {
    return _instance;
  }

  TelemetryStore._privateConstructor() {
    //startTimer();
  }
}

abstract class _TelemetryStore with Store implements TelemetryDecoderDelegate {
  @observable
  // ObservableMap<int, Sensor> sensors = ObservableMap<int, Sensor>();
  ObservableList<Sensor> sensors = ObservableList<Sensor>();

  @observable
  bool isTelemetryActive;

  @observable
  bool isScanning;

  bool get isCommunicationActive => _currentMessage != null;

  TelemetryEncoder _encoder;

  TelemetryDecoder _decoder;

  TelemetryAction _currentMessage;

  Map<int, SensorMeasurement> _subscriptions;

  _TelemetryStore()
      : isTelemetryActive = false,
        isScanning = false,
        _encoder = TelemetryEncoder(),
        _currentMessage = null,
        _subscriptions = Map(),
        super() {
    _decoder = TelemetryDecoder(this);
    BluetoothStore.instance.frameDecoder.registerPayloadDecoder(_decoder);
    print(
        "[TelemetryStore] Subscribe to bluetooth status ${BluetoothStore.instance.bluetoothStatus}");
  }

  @action
  void updateBluetoothStatus(BluetoothStatus bluetoothStatus) {
    log("[TelemetryStore] Got bluetooth state update $bluetoothStatus",
        LogLevel.DEBUG);
    if (bluetoothStatus != BluetoothStatus.CONNECTED) {
      _currentMessage = null;
      isScanning = false;
      isTelemetryActive = false;
    }
  }

  @action
  void updateMeasurement(int subscriptionId, double value) {
    // sensors[subscriptionId]?.value = value;
  }

  @action
  void setTelemetryActiveState(bool isActive) {
    log("[TelemetryStore] Set telemetry active to $isActive", LogLevel.DEBUG);
    if (isActive) {
      _currentMessage = TelemetryAction.ACTIVATE;
      BluetoothStore.instance.sendBuffer(_encoder.activateTelemetyExchange());
    } else {
      isTelemetryActive = false;
    }
  }

  @action
  void telemetryActivationConfirmed() {
    log("[TelemetryStore] Telemetry activation confirmed", LogLevel.DEBUG);
    isTelemetryActive = true;
    _currentMessage = null;
  }

  @action
  void scanConfirmed() {
    isScanning = true;
    _currentMessage = null;
  }

  @action
  void cancelScanConfirmed() {
    isScanning = false;
    _currentMessage = null;
  }

  void subscribeToBluetoothStatus() {
    try {
      BluetoothStore.instance.bluetoothStatus.listen(updateBluetoothStatus);
    } catch (e) {
      log("Failed to subscript to bluetooth status $e", LogLevel.ERROR);
    }
  }

  void startScan() {
    log("[TelemetryStore] Start scan", LogLevel.DEBUG);
    BluetoothStore.instance.sendBuffer(_encoder.startScan());
    _currentMessage = TelemetryAction.REQUEST_SCAN;
  }

  void stopScan() {
    log("[TelemetryStore] Stop scan", LogLevel.DEBUG);
    BluetoothStore.instance.sendBuffer(_encoder.stopScan());
    _currentMessage = TelemetryAction.CANCEL_SCAN;
  }

  void receivedAck() {
    log("[TelemetryStore] Received ACK", LogLevel.DEBUG);
    switch (_currentMessage) {
      case TelemetryAction.ACTIVATE:
        return telemetryActivationConfirmed();
      case TelemetryAction.REQUEST_SCAN:
        return scanConfirmed();
        break;
      case TelemetryAction.CANCEL_SCAN:
        return cancelScanConfirmed();
      case TelemetryAction.IDENTIFIED_SENSORS:
        _currentMessage = null;
        return;
      case TelemetryAction.SUBSCRIBE:
        return;
    }
  }

  void receivedNack() {
    log("[TelemetryStore] Received NACK", LogLevel.DEBUG);
    switch (_currentMessage) {
      case TelemetryAction.ACTIVATE:
      // TODO
      case TelemetryAction.REQUEST_SCAN:
      // TODO
      case TelemetryAction.CANCEL_SCAN:
      // TODO
      case TelemetryAction.IDENTIFIED_SENSORS:
      // TODO
      case TelemetryAction.SUBSCRIBE:
        // TODO
        return;
      case TelemetryAction.UNSUBSCRIBE:
        log("[TelemetryStore] Failed to unsubscripe sensor", LogLevel.DEBUG);
        return;
    }
  }

  Sensor getSensor(int manufacturerId, int deviceId) {
    final sensorIterator = sensors.where((sensor) =>
        sensor.manufacturerId == manufacturerId && sensor.deviceId == deviceId);
    return sensorIterator.length == 1 ? sensorIterator.first : null;
  }

  @action
  void addSensor(Sensor sensor) {
    if (getSensor(sensor.manufacturerId, sensor.deviceId) != null) {
      log("[TelemetryStore] Sensor $sensor has been identified", LogLevel.INFO);
      return;
    }

    log("[TelemetryStore] Add sensor $sensor", LogLevel.INFO);
    sensors.add(sensor);
  }

  void addMeasurementToSensor(int manufacturerId, int deviceId, int sensorId,
      String description, String unit) {
    final Sensor sensor = getSensor(manufacturerId, deviceId);

    if (sensor == null) {
      log("[TelemetryStore] No sensor found with manufacturerId $manufacturerId and deviceId $deviceId",
          LogLevel.INFO);
      log("[TelemetryStore] Drop sensor measurement", LogLevel.INFO);
      return;
    }

    final SensorMeasurement measurement =
        SensorMeasurement(sensor, sensorId, description, unit);
    log("[TelemetryStore] Add sensor measurement $measurement", LogLevel.INFO);
    sensor.addMeasurement(measurement);
  }

  @override
  Future<void> sensorIdentified(int manufacturerId, int deviceId, int sensorId,
      String title, String subtitle) async {
    if (sensorId == 0) {
      addSensor(Sensor(manufacturerId, deviceId, title, subtitle));
      return;
    }

    addMeasurementToSensor(manufacturerId, deviceId, sensorId, title, subtitle);
  }

  void pullIdentifiedSensors() {
    // if (_currentMessage == TelemetryAction.IDENTIFIED_SENSORS) {
    //   log("[TelemetryStore] Identified sensors have already been requested",
    //       LogLevel.DEBUG);
    //   return;
    // }

    if (_currentMessage != null &&
        _currentMessage != TelemetryAction.IDENTIFIED_SENSORS) {
      return;
    }

    log("[TelemetryStore] Pull identified sensors", LogLevel.DEBUG);
    BluetoothStore.instance.sendBuffer(_encoder.pullIdentifiedSensors());
    _currentMessage = TelemetryAction.IDENTIFIED_SENSORS;
  }

  void confirmSubscription(
      int manufacturerId, int deviceId, int sensorId, int subscriptionId) {
    log("[TelemetryStore] Subscription confirmed", LogLevel.DEBUG);
    Sensor sensor = getSensor(manufacturerId, deviceId);
  }

  void subscribeSensor(SensorMeasurement measurement) {
    log("[TelemetryStore] Subscribe to sensor $measurement", LogLevel.DEBUG);

    int subscriptionId =
        _subscriptions.keys.length > 0 ? _subscriptions.keys.reduce(max) : 0;
    if (subscriptionId == 255) {
      log("[TelemetryStore] Subscription limit reached", LogLevel.ERROR);
      return;
    }

    _currentMessage = TelemetryAction.SUBSCRIBE;
    BluetoothStore.instance.sendBuffer(_encoder.subscribeToSensor(
        measurement.manufacturerId,
        measurement.deviceId,
        measurement.sensorId,
        ++subscriptionId));
  }

  @override
  void sensorSubscriptionConfirmed(
      int manufacturerId, int deviceId, int sensorId, int subscriptionId) {
    log("[TelemetryStore] Subscription confirmed", LogLevel.INFO);
    final Sensor sensor = getSensor(manufacturerId, deviceId);
    final SensorMeasurement measurement = sensor.getSensorMeasurement(sensorId);

    if (_subscriptions.containsKey(subscriptionId)) {
      log("[TelemetryStore] Overwrite subscription $subscriptionId",
          LogLevel.DEBUG);
    }

    _subscriptions[subscriptionId] = measurement;
    measurement.subscribed(subscriptionId);
  }

  void unsubscribeSensor(SensorMeasurement measurement) {
    log("[TelemetryStore] Unsubscript measurement $measurement",
        LogLevel.DEBUG);

    BluetoothStore.instance.sendBuffer(_encoder.unsubscribeSensor(
      measurement.manufacturerId,
      measurement.deviceId,
      measurement.sensorId,
    ));
  }

  @override
  void sensorSubscriptionCancelled(
      int manufacturerId, int deviceId, int sensorId, int subscriptionId) {
    if (!_subscriptions.containsKey(subscriptionId)) {
      log("[TelemetryStore] Failed to cancel subscription with id $subscriptionId",
          LogLevel.DEBUG);
      return;
    }

    SensorMeasurement measurement = _subscriptions[subscriptionId];
    measurement.unsubscribed();
    _subscriptions.remove(subscriptionId);
  }

  Future<void> timedTelemetryAction() async {
    if (!isTelemetryActive) {
      return;
    }

    log("[TelemetryStore] Fire time triggered telemetry action",
        LogLevel.DEBUG);

    if (isScanning) {
      pullIdentifiedSensors();
      return;
    }

    pullSubscriptions();
  }

  Future<void> pullSubscriptions() async {
    log("[TelemetryStore] Pull active subscriptions", LogLevel.DEBUG);
    if (isTelemetryActive && _subscriptions.length > 0) {
      BluetoothStore.instance.sendBuffer(_encoder.pullActiveSubscriptions());
    }
  }

  void reset() {
    log("[TelemetryStore] Reset", LogLevel.DEBUG);
    this._currentMessage = null;
    this._subscriptions.clear();
    this.isScanning = false;
    this.isTelemetryActive = false;
    this.sensors.clear();
  }

  void updateFloatSubscription(int subscriptionId, double value) {
    log("[TelemetryStore] Update subscription $subscriptionId", LogLevel.DEBUG);
    final SensorMeasurement measurement = _subscriptions[subscriptionId];
    if (measurement == null) {
      log("[TelemetryStore] Failed to update subscription $subscriptionId",
          LogLevel.ERROR);
      return;
    }
    if (measurement.sensorValue == null) {
      measurement.sensorValue = SensorFloatValue(value);
      return;
    }
    (measurement.sensorValue as SensorFloatValue).setValue(value);
  }

  void updateGpsSubscription(
      int subscriptionId, double position, int orientation) {
    log("[TelemetryStore] Update subscription $subscriptionId", LogLevel.DEBUG);
  }

  void updateDateSubscription(
      int subscriptionId, int hour, int minute, int second) {
    log("[TelemetryStore] Update subscription $subscriptionId", LogLevel.DEBUG);
  }

  void updateTimeSubscription(
      int subscriptionId, int day, int month, int year) {
    log("[TelemetryStore] Update subscription $subscriptionId", LogLevel.DEBUG);
  }
}
