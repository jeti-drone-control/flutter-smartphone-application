# Jetter

This application has the purpose to visualize telemetry values
and to generate control signals to a drone. It has been designed
in combination with following components:

- HM-10 Bluetooth module
- STM32 F4 Discovery
- Jeti Model TU2 Ex

The communication is based on BLE with the HM-10 module. References to the repository including the project to flash the STM32 F4 Discovery Board will be added here.

Please be aware that this app is not executable on emulators

# Setup
The setup of this project requires proper configuration of flutter. You can check your flutter configuration. You can check it by running `flutter doctor`. 

To run the application on you smartphone, please follow subsequent steps. These commands have to be executed in the root directory of this project.

1. Download dependencies `flutter pub get`
2. Building stores for mobx with the command `flutter pub run build_runner build --delete-conflicting-outputs`. If you want to watch for changes and contiously rebuild, run `flutter pub run build_runner watch --delete-conflicting-outputs`.
3. Feel free to start the application on your device either via IDE or CLI. Be aware: The app will crash on emulators!
