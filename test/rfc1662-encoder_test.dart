import 'package:jetter/core/rfc1662-protocol/rfc1662-encoder.dart';
import 'package:test/test.dart';

void main() {
  group('Framing', () {
    test('Expect protocol ID', () {
      List<int> payload = [0xAA];
      List<int> messageFrame = Rfc1662Encoder().encodeFrame(1, payload);
      expect(messageFrame[1], 1);
    });

    test('Expect payload length', () {
      List<int> payload = [0xAA];
      List<int> messageFrame = Rfc1662Encoder().encodeFrame(1, payload);
      expect(messageFrame[2], 1);
    });

    test('Expect escaped payload length', () {
      List<int> payload = [0x7D];
      List<int> messageFrame = Rfc1662Encoder().encodeFrame(1, payload);
      expect(messageFrame[2], 2);
    });

    test('Expect framing around payload', () {
      List<int> payload = [0xAA];
      List<int> messageFrame = Rfc1662Encoder().encodeFrame(0, payload);
      expect(messageFrame[0], 126);
      expect(messageFrame.length, 5);
      // TODO: Test crc
    });
  });
  group('Escaping of values', () {
    test('Expect unescaped payload', () {
      List<int> payload = [0xFF];
      List<int> messageFrame = Rfc1662Encoder().encodeFrame(0, payload);
      expect(messageFrame[3], 0xFF);
    });
    test('Expect frame indicator to be escaped', () {
      List<int> payload = [0x7E];
      List<int> messageFrame = Rfc1662Encoder().encodeFrame(0, payload);
      expect(messageFrame[3], 0x7D);
      expect(messageFrame[4], 0x5E);
    });

    test('Expect escape flag to be escaped', () {
      List<int> payload = [0x7D];
      List<int> messageFrame = Rfc1662Encoder().encodeFrame(0, payload);
      expect(messageFrame[3], 0x7D);
      expect(messageFrame[4], 0x5D);
    });

    test('Expect multiple escpaes', () {
      List<int> payload = [0x7E, 0xAA, 0xBB, 0x7D];
      List<int> messageFrame = Rfc1662Encoder().encodeFrame(0, payload);
      expect(messageFrame[3], 0x7D);
      expect(messageFrame[4], 0x5E);
      expect(messageFrame[7], 0x7D);
      expect(messageFrame[8], 0x5D);
    });
  });

  group('CRC calculation', () {
    test('Expect CRC calculation without escapes', () {
      List<int> payload = [0x0A, 0x0B, 0x0C, 0x0D, 0xFF];
      List<int> messageFrame = Rfc1662Encoder().encodeFrame(0, payload);
      expect(messageFrame.last, 0x3A);
    });

    test('Expect CRC calculation of escaped values', () {
      List<int> payload = [0x01, 0x7E];
      List<int> messageFrame = Rfc1662Encoder().encodeFrame(0, payload);
      expect(messageFrame.last, 0x87);
    });
  });
}
